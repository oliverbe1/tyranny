#include <SFML/Graphics.hpp>
#include "game/Game.h"
#include "game/objects/GameObject.h"
#include "Renderer.h"
#include "RenderScreen.h"

namespace ty {
namespace sfml {

//////////////////////////////////////////////////////////////////////
Renderer::Renderer( sf::RenderWindow& window, int pixelsPerUnit )
: m_window(window), m_pixelsPerUnit(pixelsPerUnit) { }

//////////////////////////////////////////////////////////////////////
void Renderer::render( Game& game )
{
    RenderScreen renderScreen(m_window, m_pixelsPerUnit);
    game.getCurrentScreen().applyAction(renderScreen);
}

//////////////////////////////////////////////////////////////////////
sf::RenderWindow& Renderer::window()
{ return m_window; }

} // end ns sfml
} // end ns ty

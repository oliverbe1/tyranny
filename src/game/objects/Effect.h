#ifndef INC_EFFECT_H
#define INC_EFFECT_H

#include "GameObject.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
/// An effect is a game object which has no direct influence 
/// on the outcome of a game. E.g. an explosion.
//////////////////////////////////////////////////////////////////////
class Effect : public GameObject
{
public:
    enum Graphic { EXPLOSION, BULLET_HIT };

    //////////////////////////////////////////////////////////////////
    /// Creates a new effect with given graphic.
    //////////////////////////////////////////////////////////////////
    Effect( Graphic g );

    //////////////////////////////////////////////////////////////////
    /// Returns the graphic of this effect.
    //////////////////////////////////////////////////////////////////
    Graphic getGraphic() const;

    //////////////////////////////////////////////////////////////////
    /// Returns how long this effect will last compared to its maximum
    /// duration. As a ratio between 0 and 1.
    //////////////////////////////////////////////////////////////////
    float   getRemaining() const;

    virtual void update( float timeDelta, BulletFactory& factory );
    virtual void applyAction( GameObjectAction& a );

private:
    Graphic m_graphic;
    float   m_duration;
    float   m_remaining;
};

} // end ns

#endif

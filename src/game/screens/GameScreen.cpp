#include <fstream>
#include <iostream>
#include <memory>
#include <string>
#include "game/Action.h"
#include "game/objects/PlayerShip.h"
#include "game/world/World.h"
#include "game/world/WorldParser.h"
#include "Config.h"
#include "GameScreen.h"
#include "GameLostScreen.h"
#include "GameFinishedScreen.h"

namespace ty {
using namespace std;

static const float kRespawnCooldown   = 2.5;
static const float kManualRespawnFrom = 2;
static const float kLives             = 3;

//////////////////////////////////////////////////////////////////////
GameScreen::GameScreen( int worldId )
: m_worldId(worldId)
, m_nextScreen(nullptr)
, m_respawnCooldown(0)
, m_remainingLives(kLives)
{ 
    loadWorld(worldId);
}

//////////////////////////////////////////////////////////////////////
const World& GameScreen::getWorld() const
{ return *m_world; }

int GameScreen::getRemainingLives() const
{ return m_remainingLives; }

//////////////////////////////////////////////////////////////////////
void GameScreen::processAction( const Action& action )
{
    // Commands
    if( action == Action::NEXT_LEVEL ) {
        if( worldExists(m_worldId+1) ) {
            loadWorld(m_worldId+1);
        } else {
            m_nextScreen = unique_ptr<Screen>(new GameFinishedScreen());
        }
    } else
    if( action == Action::RESTART_LEVEL ) {
        loadWorld(m_worldId);
    } else
    if( action == Action::SKIP_WAVE ) {
        m_world->skipWave();
    }

    // Player actions
    if( m_world->getPlayer()->isDestroyed() ) { return; }

    if( action == Action::LEFT ) {
        m_world->getPlayer()->moveLeft();
    } else
    if( action == Action::RIGHT ) {
        m_world->getPlayer()->moveRight();
    } else
    if( action == Action::UP ) {
        m_world->getPlayer()->moveUp();
    } else
    if( action == Action::DOWN ) {
        m_world->getPlayer()->moveDown();
    } else
    if( action == Action::SHOOT_OR_SELECT ) {
        m_world->getPlayer()->shoot();
    }
}

//////////////////////////////////////////////////////////////////////
void GameScreen::update( float timeDelta )
{
    if( m_nextScreen ) { return; }

    // If player dies, respawn at start of current wave.
    if( m_respawnCooldown > 0 ) {
        m_respawnCooldown -= timeDelta;
        if( m_respawnCooldown <= 0 && m_remainingLives > 0 ) {
            loadWorld(m_worldId, m_world->getCurrentWave());
        } else
        if( m_respawnCooldown <= 0 && m_remainingLives <= 0 ) {
            m_nextScreen = unique_ptr<Screen>(new GameLostScreen());
        }
    } else
    if( m_world->getPlayer()->isDestroyed() ) {
        m_remainingLives -= 1;
        m_respawnCooldown = kRespawnCooldown;
    }

    // If world is finished, load next world
    if( m_world->isCompleted() ) {
        if( worldExists(m_worldId+1) ) {
            loadWorld(m_worldId+1);
        } else {
            m_nextScreen = unique_ptr<Screen>(new GameFinishedScreen());
        }
    // Otherwise, update the world
    } else {
        m_world->update(timeDelta);
    }
}

//////////////////////////////////////////////////////////////////////
unique_ptr<Screen> GameScreen::nextScreen()
{
    return std::move(m_nextScreen);
}

// Private
//////////////////////////////////////////////////////////////////////
void GameScreen::loadWorld( int worldId, int wave )
{
    // Read world
    WorldParser parser;
    string fname = kResourceLocation + "world" + to_string(worldId) + ".tyw";
    m_world = unique_ptr<World>(new World(parser.readFromFile(fname)));
    m_worldId = worldId;
    m_respawnCooldown = 0;
    // Skip to given wave in world
    for( int i = 1; i < wave; ++i ) { m_world->skipWave(); }
}

//////////////////////////////////////////////////////////////////////
bool GameScreen::worldExists( int worldId ) const
{
    string fname = kResourceLocation + "world" + to_string(worldId) + ".tyw";
    ifstream ifs(fname);
    return ifs.good();
}

//////////////////////////////////////////////////////////////////////
void GameScreen::applyAction( ScreenAction& a )
{ a.applyTo(*this); }


} // end ns

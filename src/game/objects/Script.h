#ifndef INC_SCRIPT_H
#define INC_SCRIPT_H
#include <memory>
#include <vector>
#include "math/Vector.h"

namespace ty {
class EnemyShip;
using std::vector;
using std::unique_ptr;

//////////////////////////////////////////////////////////////////////
/// Interface for defining enemy unit scripts.
///
/// A Script is some sequence of actions an enemy unit should follow,
/// such as moving around the field in a specific pattern.
/// Scripts are composable: they can be run in sequence or looped any
/// number of times.
/// Scripts can be directly defined in the TyRANNY World file format, 
/// see `WorldParser` for more information.
//////////////////////////////////////////////////////////////////////
class Script
{
public:
    //////////////////////////////////////////////////////////////////
    /// Simulates the script for given time duration in seconds.
    /// Note that you should not call the ship's update method
    /// manually in here, the game loop will take care of this.
    //////////////////////////////////////////////////////////////////
    virtual void execute( EnemyShip& ship, float timeDelta ) = 0;

    //////////////////////////////////////////////////////////////////
    /// Resets the script to its state as it was before execute()
    /// was first called.
    //////////////////////////////////////////////////////////////////
    virtual void reset() = 0;

    //////////////////////////////////////////////////////////////////
    /// Indicates whether the script has fully completed.
    //////////////////////////////////////////////////////////////////
    virtual bool isFinished() const = 0;

    //////////////////////////////////////////////////////////////////
    /// Returns a copy of this script.
    //////////////////////////////////////////////////////////////////
    virtual unique_ptr<Script> clone() const = 0;

    virtual ~Script() { }
};

//////////////////////////////////////////////////////////////////////
/// A Script for running multiple other scripts consecutively.
/// As soon as the current script indicates it is finished, the next 
/// script will be run.
//////////////////////////////////////////////////////////////////////
class Sequence : public Script
{
    vector<unique_ptr<Script>> m_scripts;
    size_t m_currentScript;
public:
    Sequence();
    void addScript( unique_ptr<Script> script );
    virtual void execute( EnemyShip& ship, float timeDelta );
    virtual void reset();
    virtual bool isFinished() const;
    virtual unique_ptr<Script> clone() const;
};

//////////////////////////////////////////////////////////////////////
/// A Script for looping another script a set number of times.
//////////////////////////////////////////////////////////////////////
class Loop : public Script
{
    unique_ptr<Script> m_script;
    int m_numIterations;
    int m_remainingIterations;
public:
    Loop( unique_ptr<Script> script, int numIterations );
    virtual void execute( EnemyShip& ship, float timeDelta );
    virtual void reset();
    virtual bool isFinished() const;
    virtual unique_ptr<Script> clone() const;
};

//////////////////////////////////////////////////////////////////////
/// Script for having a unit follow a fixed path.
///
/// The given path is relative to the enemy unit's position when
/// execute() is first called.
//////////////////////////////////////////////////////////////////////
class FollowPath : public Script
{
    vector<Vector2f> m_relativePath;
    vector<Vector2f> m_absolutePath;
    vector<float>    m_speedMultipliers;
    bool   m_absPathCalculated;
    size_t m_curGoal;
public:
    FollowPath( vector<Vector2f> relativePath );
    FollowPath( vector<Vector2f> relativePath, 
                vector<float> speedMultipliers );
    virtual void execute( EnemyShip& ship, float timeDelta );
    virtual void reset();
    virtual bool isFinished() const;
    virtual unique_ptr<Script> clone() const;
private:
    void calculateAbsolutePath( EnemyShip& ship );
};

//////////////////////////////////////////////////////////////////////
/// Script for having an enemy unit idle/wait for some amount of time.
///
/// This will cancel any movement, but will not interrupt shooting.
//////////////////////////////////////////////////////////////////////
class Idle : public Script
{
    float m_duration;
    float m_remaining;
public:
    Idle( float duration ); ///< idle for duration seconds
    virtual void execute( EnemyShip& ship, float timeDelta );
    virtual void reset();
    virtual bool isFinished() const;
    virtual unique_ptr<Script> clone() const;
};

//////////////////////////////////////////////////////////////////////
/// Script which instructs enemy ship to stop shooting when executed.
//////////////////////////////////////////////////////////////////////
class StopShooting : public Script
{
    bool m_done;
public:
    StopShooting( );
    virtual void execute( EnemyShip& ship, float timeDelta );
    virtual void reset();
    virtual bool isFinished() const;
    virtual unique_ptr<Script> clone() const;
};

//////////////////////////////////////////////////////////////////////
/// Script which instructs enemy ship to start shooting when executed.
//////////////////////////////////////////////////////////////////////
class StartShooting : public Script
{
    bool m_done;
public:
    StartShooting( );
    virtual void execute( EnemyShip& ship, float timeDelta );
    virtual void reset();
    virtual bool isFinished() const;
    virtual unique_ptr<Script> clone() const;
};

} // end ns

#endif

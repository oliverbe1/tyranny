#include <SFML/Graphics.hpp>
#include <stdexcept>
#include "Assets.h"
#include "Config.h"

namespace ty {
namespace sfml {

//////////////////////////////////////////////////////////////////////
Assets& assets() 
{
    static Assets assets;
    return assets;
}

//////////////////////////////////////////////////////////////////////
Assets::Assets()
{
    // Bullets
    T_BULLET_BALL             = loadTexture("bullet_ball");
    BULLET_BALL               = sf::Sprite(T_BULLET_BALL);
    BULLET_BALL               . setOrigin(sf::Vector2f(11,10));
    T_BULLET_BOMB             = loadTexture("bullet_bomb");
    BULLET_BOMB               = sf::Sprite(T_BULLET_BOMB);
    BULLET_BOMB               . setOrigin(sf::Vector2f(20,51));
    T_BULLET_HEART            = loadTexture("bullet_heart");
    BULLET_HEART              = sf::Sprite(T_BULLET_HEART);
    BULLET_HEART              . setOrigin(sf::Vector2f(15,15));
    T_BULLET_PLAYER           = loadTexture("bullet_player");
    BULLET_PLAYER             = sf::Sprite(T_BULLET_PLAYER);
    BULLET_PLAYER             . setOrigin(sf::Vector2f(9,10));
    T_BULLET_TEAR             = loadTexture("bullet_tear");
    BULLET_TEAR               = sf::Sprite(T_BULLET_TEAR);
    BULLET_TEAR               . setOrigin(sf::Vector2f(10,19));
    T_BULLET_TOXIC            = loadTexture("bullet_toxic");
    BULLET_TOXIC              = sf::Sprite(T_BULLET_TOXIC);
    BULLET_TOXIC              . setOrigin(sf::Vector2f(15,15));

    // Player
    T_PLAYER                  = loadTexture("player");
    PLAYER                    = sf::Sprite(T_PLAYER);
    PLAYER                    . setOrigin(sf::Vector2f(22, 30));

    // Enemies
    T_ENEMY_MADBOMBER         = loadTexture("enemy_madbomber");
    ENEMY_MADBOMBER           = sf::Sprite(T_ENEMY_MADBOMBER);
    ENEMY_MADBOMBER           . setOrigin(sf::Vector2f(96, 68));
    T_ENEMY_NOVADESTROYER     = loadTexture("enemy_novadestroyer");
    ENEMY_NOVADESTROYER       = sf::Sprite(T_ENEMY_NOVADESTROYER);
    ENEMY_NOVADESTROYER       . setOrigin(sf::Vector2f(46, 44));
    T_ENEMY_SCOUT             = loadTexture("enemy_scout");
    ENEMY_SCOUT               = sf::Sprite(T_ENEMY_SCOUT);
    ENEMY_SCOUT               . setOrigin(sf::Vector2f(32, 32));
    T_ENEMY_SPIRALSLINGER     = loadTexture("enemy_spiralslinger");
    ENEMY_SPIRALSLINGER       = sf::Sprite(T_ENEMY_SPIRALSLINGER);
    ENEMY_SPIRALSLINGER       . setOrigin(sf::Vector2f(46, 45));
    T_ENEMY_UNSTABLEPROTECTOR = loadTexture("enemy_unstableprotector");
    ENEMY_UNSTABLEPROTECTOR   = sf::Sprite(T_ENEMY_UNSTABLEPROTECTOR);
    ENEMY_UNSTABLEPROTECTOR   . setOrigin(sf::Vector2f(25, 25));

    // Powerups
    T_POWERUP_SCATTERSHOT     = loadTexture("powerup_scattershot");
    POWERUP_SCATTERSHOT       = sf::Sprite(T_POWERUP_SCATTERSHOT);
    POWERUP_SCATTERSHOT       . setOrigin(sf::Vector2f(28, 29));
    T_POWERUP_YCOMBINATOR     = loadTexture("powerup_ycombinator");
    POWERUP_YCOMBINATOR       = sf::Sprite(T_POWERUP_YCOMBINATOR);
    POWERUP_YCOMBINATOR       . setOrigin(sf::Vector2f(28, 29));
    T_POWERUP_SLOWMO          = loadTexture("powerup_slowmo");
    POWERUP_SLOWMO            = sf::Sprite(T_POWERUP_SLOWMO);
    POWERUP_SLOWMO            . setOrigin(sf::Vector2f(28, 29));

    // Effects
    T_EFFECT_EXPLOSION        = loadTexture("effect_explosion");
    EFFECT_EXPLOSION          = sf::Sprite(T_EFFECT_EXPLOSION);
    EFFECT_EXPLOSION          . setOrigin(sf::Vector2f(60, 60));

    // Tiles
    T_TILE_BROWN              = loadTexture("tile_brown");
    TILE_BROWN                = sf::Sprite(T_TILE_BROWN);
    T_TILE_BROWN_SLANTED      = loadTexture("tile_brown_slanted");
    TILE_BROWN_SLANTED        = sf::Sprite(T_TILE_BROWN_SLANTED);
    T_TILE_PLATE_BLUE         = loadTexture("tile_plate_blue");
    TILE_PLATE_BLUE           = sf::Sprite(T_TILE_PLATE_BLUE);
    T_TILE_PLATE_GREEN        = loadTexture("tile_plate_green");
    TILE_PLATE_GREEN          = sf::Sprite(T_TILE_PLATE_GREEN);
    T_TILE_SQUARES            = loadTexture("tile_squares");
    TILE_SQUARES              = sf::Sprite(T_TILE_SQUARES);

    // Menus
    T_MENU_MAIN_HARD          = loadTexture("menu_main_hard");
    MENU_MAIN_HARD            = sf::Sprite(T_MENU_MAIN_HARD);
    T_MENU_MAIN_HARDER        = loadTexture("menu_main_harder");
    MENU_MAIN_HARDER          = sf::Sprite(T_MENU_MAIN_HARDER);
    T_MENU_WON                = loadTexture("menu_won");
    MENU_WON                  = sf::Sprite(T_MENU_WON);
    T_MENU_LOST               = loadTexture("menu_lost");
    MENU_LOST                 = sf::Sprite(T_MENU_LOST);
    T_GUI_LIVES_0             = loadTexture("gui_lives_0");
    GUI_LIVES_0               = sf::Sprite(T_GUI_LIVES_0);
    GUI_LIVES_0               . setOrigin(sf::Vector2f(85, 0));
    T_GUI_LIVES_1             = loadTexture("gui_lives_1");
    GUI_LIVES_1               = sf::Sprite(T_GUI_LIVES_1);
    GUI_LIVES_1               . setOrigin(sf::Vector2f(85, 0));
    T_GUI_LIVES_2             = loadTexture("gui_lives_2");
    GUI_LIVES_2               = sf::Sprite(T_GUI_LIVES_2);
    GUI_LIVES_2               . setOrigin(sf::Vector2f(85, 0));
    T_GUI_LIVES_3             = loadTexture("gui_lives_3");
    GUI_LIVES_3               = sf::Sprite(T_GUI_LIVES_3);
    GUI_LIVES_3               . setOrigin(sf::Vector2f(85, 0));
}

//////////////////////////////////////////////////////////////////////
sf::Texture Assets::loadTexture( const std::string& name ) const
{
    std::string fname = kResourceLocation + "images/" + name + ".png";
    sf::Texture texture;
    if( !texture.loadFromFile(fname) ) {
        throw std::runtime_error("unable to load image " + fname +
                                 ". Make sure the working directory contains the resources/ folder.");
    }
    return texture;
}

} // End ns sfml
} // End ns ty

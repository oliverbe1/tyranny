#ifndef INC_BULLET_H
#define INC_BULLET_H
#include "GameObject.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
/// A bullet object.
///
/// Bullets can be friendly or hostile and inflict a specific amount 
/// of damage when a target is hit.
//////////////////////////////////////////////////////////////////////
class Bullet : public GameObject
{
public:
    enum Graphic { BALL, BOMB, HEART, PLAYER, TEAR, TOXIC };
private:
    Graphic m_graphic;
    bool    m_friendly;
    float   m_damage;
public:
    Bullet( Graphic g, bool friendly, float damage, float radius );

    Graphic getGraphic() const;
    bool    isFriendly() const;
    float   getDamage()  const;

    virtual void applyAction( GameObjectAction& a );
};

} // end ns

#endif

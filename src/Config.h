#ifndef INC_CONFIG_H
#define INC_CONFIG_H
#include <string>

namespace ty {

// Display configuration
//
// Be careful tweaking these! World scripts and images are designed 
// to work well with these settings.
//////////////////////////////////////////////////////////////////////
constexpr int kWindowWidth  = 720; // px
constexpr int kWindowHeight = 560;  
constexpr int kTileSize     = 80;  
constexpr int kFrameTime    = 15;  // ms

static_assert(kWindowWidth % kTileSize == 0, 
        "window width must be multiple of tile size");
static_assert(kWindowHeight % kTileSize == 0, 
        "window height must be multiple of tile size");
static_assert(kWindowWidth / kTileSize == 9, 
        "game is played on a field of exactly 9 tiles wide");

// Resource Location, relative to current working directory
//////////////////////////////////////////////////////////////////////
const std::string kResourceLocation = "resources/";

} // end ns

#endif

#ifndef INC_WORLDPARSER_H
#define INC_WORLDPARSER_H
#include "World.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
/// A Parser for Tyranny's World Format (.tyw)
///
/// For a full description of the format, see resources/Readme.txt.
//////////////////////////////////////////////////////////////////////
class WorldParser
{
public:
    WorldParser();

    World readFromFile( const std::string& name );
};

} // end ns

#endif

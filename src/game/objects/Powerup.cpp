#include "math/Vector.h"
#include "game/world/World.h"
#include "Powerup.h"
#include "PlayerShip.h"

namespace ty {

static const float kPowerupSpeed    = 1.1;
static const float kPowerupRadius   = 0.39;
static const float kSlowmoFactor    = 0.55;
static const float kSlowmoDuration  = 10.0;

//////////////////////////////////////////////////////////////////////
Powerup::Powerup() : GameObject(kPowerupRadius) 
{ setVelocity(Vector2f(0,-kPowerupSpeed)); }

//////////////////////////////////////////////////////////////////////
WeaponPowerup::WeaponPowerup( Weapon wpn ) : m_wpn(wpn) { }

//////////////////////////////////////////////////////////////////////
Weapon WeaponPowerup::getWeapon() const
{ return m_wpn; }

//////////////////////////////////////////////////////////////////////
void WeaponPowerup::onPickup( World& world )
{ world.getPlayer()->changeWeapon(m_wpn); }

//////////////////////////////////////////////////////////////////////
void WeaponPowerup::applyAction( GameObjectAction& a )
{ a.applyTo(*this); }


//////////////////////////////////////////////////////////////////////
SlowmoPowerup::SlowmoPowerup( ) : m_factor(kSlowmoFactor) { }
SlowmoPowerup::SlowmoPowerup( float factor ) : m_factor(factor) { }

//////////////////////////////////////////////////////////////////////
void SlowmoPowerup::onPickup( World& world )
{ world.slowTime(m_factor, kSlowmoDuration); }

//////////////////////////////////////////////////////////////////////
void SlowmoPowerup::applyAction( GameObjectAction& a )
{ a.applyTo(*this); }

} // end ns

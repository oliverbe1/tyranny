#include <cmath>
#include <memory>
#include "BulletFactory.h"
#include "SpiralSlinger.h"
#include "Script.h"

namespace ty {
using namespace std;

//////////////////////////////////////////////////////////////////////
SpiralSlinger::SpiralSlinger( unique_ptr<Script> script )
: EnemyShip( 0.7,    // base speed
             210,  // hit points
             0.26,  // weapon cooldown
             0,    // shooting angle
             0.45,  // radius
             std::move(script) )
{ }

//////////////////////////////////////////////////////////////////////
void SpiralSlinger::applyAction( GameObjectAction& a )
{ a.applyTo(*this); }

//////////////////////////////////////////////////////////////////////
void SpiralSlinger::shoot( BulletFactory& factory )
{
    static const float pi = 3.14159265359;
    for( float i = 0; i < 1; i += 0.251 ) {
        float angle = 2*pi * (i + getShootingAngle()/360.);
        float px = std::cos(angle) * getRadius();
        float py = std::sin(angle) * getRadius();
        Vector2f pos = getPosition() + 1.2f*Vector2f(px, py);
        Vector2f vel = Vector2f(4*px, 4*py);
        factory.spawnHostileBullet(pos, vel, Bullet::TOXIC, 0.15);
    }
    float newAngle = getShootingAngle() + 15;
    if( newAngle > 360 ) { newAngle -= 360; }
    setShootingAngle(newAngle);
}

} // end ns


#ifndef INC_BULLETFACTORY_H
#define INC_BULLETFACTORY_H
#include "math/Vector.h"
#include "Bullet.h"

namespace ty {
class World;

//////////////////////////////////////////////////////////////////////
/// Factory for creating bullets.
///
/// Ships creating bullets through the factory require no knowledge 
/// of the world they're operating in.
//////////////////////////////////////////////////////////////////////
class BulletFactory
{
    World& m_world;

public:
    BulletFactory( World& world );
    void spawnFriendlyBullet( Vector2f pos, Vector2f vel, Bullet::Graphic g, 
                              float damage, float radius );
    void spawnHostileBullet ( Vector2f pos, Vector2f vel, Bullet::Graphic g, 
                              float radius );
};

} // end ns

#endif

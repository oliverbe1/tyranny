#ifndef INC_TILE_H
#define INC_TILE_H

namespace ty {

//////////////////////////////////////////////////////////////////////
// A tile in the world's scrolling background.
//////////////////////////////////////////////////////////////////////
enum class Tile 
{ 
    SQUARES, 
    PLATE_BLUE, 
    PLATE_GREEN, 

    BROWN, 
    BROWN_SLANTED, 
};

} // end ns

#endif

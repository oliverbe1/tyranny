#ifndef INC_MADBOMBER_H
#define INC_MADBOMBER_H
#include <memory>
#include "BulletFactory.h"
#include "EnemyShip.h"

namespace ty {
class GameObjectAction;

//////////////////////////////////////////////////////////////////////
/// A boss-type enemy ship dropping large bombs.
///
/// Bombs are dropped in quick bursts from 2 cannons at the sides of
/// the ship. The direction bombs are dropped in is semi-random.
//////////////////////////////////////////////////////////////////////
class MadBomber : public EnemyShip
{
    const static float kBurstCooldown; ///< Cooldown in between bombing bursts
    const static float kBurstCount;    ///< Number of bombs in one burst attack
    const static float kBurstSpeed;    ///< Time between burst shots

    float m_burstCooldown;
    float m_burstRemaining;
public:
    MadBomber( std::unique_ptr<Script> script );
    virtual void applyAction( GameObjectAction& a ) override;
protected:
    virtual void shoot( BulletFactory& factory ) override;
};

} // end ns

#endif

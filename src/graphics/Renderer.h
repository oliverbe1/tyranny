#ifndef INC_RENDERER_H
#define INC_RENDERER_H
#include <SFML/Graphics.hpp>

namespace ty {
class Game;
namespace sfml {

//////////////////////////////////////////////////////////////////////
/// SFML based renderer for the game of Tyranny.
//////////////////////////////////////////////////////////////////////
class Renderer
{
    sf::RenderWindow& m_window;
    int m_pixelsPerUnit;

public:
    Renderer( sf::RenderWindow& window, int pixelsPerUnit );
    void render( ty::Game& game );
    sf::RenderWindow& window();
};

} // end ns sfml
} // end ns ty
#endif

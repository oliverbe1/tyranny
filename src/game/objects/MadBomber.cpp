#include <cmath>
#include <memory>
#include "math/algorithm.h"
#include "Bullet.h"
#include "BulletFactory.h"
#include "EnemyShip.h"
#include "MadBomber.h"
#include "Script.h"

namespace ty {
using namespace std;
using namespace ty::math;

const float MadBomber::kBurstCooldown = 0.35;
const float MadBomber::kBurstCount    = 4;
const float MadBomber::kBurstSpeed    = 0.35;

//////////////////////////////////////////////////////////////////////
MadBomber::MadBomber( unique_ptr<Script> script )
: EnemyShip( 2.0,          // base speed
             950,          // hit points
             kBurstSpeed,  // weapon cooldown
             0,            // shooting angle
             0.9,          // radius
             std::move(script) )
, m_burstCooldown (kBurstCooldown)
, m_burstRemaining(kBurstCount)
{ }

//////////////////////////////////////////////////////////////////////
void MadBomber::applyAction( GameObjectAction& a )
{ a.applyTo(*this); }

//////////////////////////////////////////////////////////////////////
void MadBomber::shoot( BulletFactory& factory )
{
    if( m_burstCooldown >= 0 ) {
        m_burstCooldown -= kBurstSpeed;
        return;
    }
    // Left cannon
    {
        Vector2f pos = getPosition() + Vector2f(-0.875, -1.34);
        Vector2f vel = Vector2f(randfloat()*4.0-2.2,-2.0f);
        factory.spawnHostileBullet(pos, vel, Bullet::BOMB, 0.21);
    }
    // Right cannon
    {
        Vector2f pos = getPosition() + Vector2f(+0.8125, -1.34);
        Vector2f vel = Vector2f(randfloat()*4.0-1.8,-2.0f);
        factory.spawnHostileBullet(pos, vel, Bullet::BOMB, 0.21);
    }
    // Trigger a big cooldown after a number of rounds
    m_burstRemaining -= 1;
    if( m_burstRemaining <= 0 ) {
        m_burstCooldown  = kBurstCooldown;
        m_burstRemaining = kBurstCount;
    }
}

} // end ns



#ifndef INC_RECTANGLE_H
#define INC_RECTANGLE_H
#include "Vector.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
/// Simple Rectangle Class.
//////////////////////////////////////////////////////////////////////
struct Rectangle
{
    float left, top, width, height;

    Rectangle( float left, float top, float width, float height )
        : left(left), top(top), width(width), height(height) { }

    bool inBounds( Vector2f v ) const
    { return v.x >= left && v.x <= (left + width)
          && v.y <= top  && v.y >= (top - height); }
};

} // end ns

#endif

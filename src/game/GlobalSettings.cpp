#include "GlobalSettings.h"

//////////////////////////////////////////////////////////////////////
GlobalSettings& globalSettings()
{
    static GlobalSettings settings;
    return settings;
}

//////////////////////////////////////////////////////////////////////
GlobalSettings::GlobalSettings()
: m_difficulty(GlobalSettings::Difficulty::HARD)
, m_godMode   (false)
{ }

//////////////////////////////////////////////////////////////////////
GlobalSettings::Difficulty GlobalSettings::getDifficulty() const
{ return m_difficulty; }

void GlobalSettings::setDifficulty( GlobalSettings::Difficulty d )
{ m_difficulty = d; }

//////////////////////////////////////////////////////////////////////
void GlobalSettings::toggleGodMode()
{ m_godMode = !m_godMode; }

bool GlobalSettings::godModeEnabled() const
{ return m_godMode; }

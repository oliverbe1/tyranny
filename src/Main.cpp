//////////////////////////////////////////////////////////////////////
//                                                                  //
//                             TyRANNY                              //
//                                                                  //
//                 a shmup not unlike other shmups                  //
//                                                                  //
//////////////////////////////////////////////////////////////////////

#include <SFML/Graphics.hpp>
#include <iostream>
#include <stdexcept>
#include "Config.h"
#include "game/Action.h"
#include "game/Game.h"
#include "graphics/Renderer.h"
#include "input/InputHandler.h"

using namespace ty;
using namespace ty::sfml;
void gameLoop( Game& game, Renderer& renderer, InputHandler& inputHandler );

//////////////////////////////////////////////////////////////////////
int main( int argc, char* argv[] )
{
    try {

    // Start new game
    Game game;

    // Set up rendering & input handling
    sf::VideoMode    videoMode    (kWindowWidth, kWindowHeight);
    sf::RenderWindow window       (videoMode, "TyRANNY", sf::Style::Close);
    Renderer         renderer     (window, kTileSize);
    InputHandler     inputHandler (window);

    // Start the game loop
    gameLoop(game, renderer, inputHandler);

    } 
    catch( std::runtime_error& e ) { std::cerr << "error: " << e.what() << '\n'; }
    catch( ... ) { std::cerr << "unknown error\n"; }
}

//////////////////////////////////////////////////////////////////////
void gameLoop( Game& game, Renderer& renderer, InputHandler& inputHandler )
{
    sf::Clock clock;
    sf::Time timeAccumulated = sf::Time::Zero;
    sf::Time timePerFrame = sf::milliseconds(kFrameTime);

    while( renderer.window().isOpen() )
    {
        sf::Time dt = clock.restart();
        timeAccumulated += dt;

        while( timeAccumulated > timePerFrame ) {
            timeAccumulated -= timePerFrame;
            for( const Action& a : inputHandler.readInput() ) {
                game.processAction(a);
            }
            game.update(timePerFrame.asSeconds());
        }

        //std::cout << dt.asMicroseconds()/1e3 << std::endl;
        renderer.render(game);
    }
}

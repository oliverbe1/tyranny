#ifndef INC_POWERUP_H
#define INC_POWERUP_H
#include "GameObject.h"
#include "PlayerShip.h"

namespace ty {
class World;

//////////////////////////////////////////////////////////////////////
/// Abstract base class for powerup items.
//////////////////////////////////////////////////////////////////////
class Powerup : public GameObject
{
public:
    //////////////////////////////////////////////////////////////////
    /// The action to be performed when the powerup has been picked up 
    /// by the player.
    //////////////////////////////////////////////////////////////////
    virtual void onPickup( World& world ) = 0;

    virtual ~Powerup() { }
protected:
    Powerup();

};

//////////////////////////////////////////////////////////////////////
/// Weapon powerup. When picked up, changes player ship's weapon.
//////////////////////////////////////////////////////////////////////
class WeaponPowerup : public Powerup
{
    Weapon m_wpn;
public:
    WeaponPowerup( Weapon wpn );

    Weapon getWeapon() const;

    virtual void onPickup( World& world ) override;

    virtual void applyAction( GameObjectAction& action ) override;
};

//////////////////////////////////////////////////////////////////////
/// Slow motion powerup. World-time slows down for a short period of 
/// time when picked up.
//////////////////////////////////////////////////////////////////////
class SlowmoPowerup : public Powerup
{
    float m_factor;
public:
    SlowmoPowerup( );
    SlowmoPowerup( float factor );

    virtual void onPickup( World& world ) override;

    virtual void applyAction( GameObjectAction& action ) override;
};

} // end ns

#endif


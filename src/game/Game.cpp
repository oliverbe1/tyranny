#include <iostream>
#include <memory>
#include <string>
#include <vector>
#include "world/World.h"
#include "screens/GameScreen.h"
#include "screens/MainMenuScreen.h"
#include "GlobalSettings.h"
#include "Game.h"
using namespace std;

namespace ty {

//////////////////////////////////////////////////////////////////////
Game::Game()
{
    m_currentScreen.reset(new MainMenuScreen());
}

//////////////////////////////////////////////////////////////////////
void Game::processAction( const Action& action )
{ 
    if( action == Action::TOGGLE_GODMODE ) {
        globalSettings().toggleGodMode();
        string toggle = globalSettings().godModeEnabled()?"ON\n":"OFF\n";
        cout << "GODMODE " << toggle;
    } else {
        m_currentScreen->processAction(action);
    }
}

//////////////////////////////////////////////////////////////////////
void Game::update( float timeDelta )
{ 
    auto nextScreen = m_currentScreen->nextScreen();
    if( nextScreen != nullptr ) {
        m_currentScreen = std::move(nextScreen);
    } else {
        m_currentScreen->update(timeDelta); 
    }
}

//////////////////////////////////////////////////////////////////////
Screen& Game::getCurrentScreen()
{ return *m_currentScreen; }


} //end ns

#include <memory>
#include "game/Action.h"
#include "GameLostScreen.h"
#include "MainMenuScreen.h"
#include "Screen.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
GameLostScreen::GameLostScreen() 
: m_blockInputDuration(1.5)
, m_nextScreen(nullptr)
{ }

//////////////////////////////////////////////////////////////////////
void GameLostScreen::processAction( const Action& action )
{
    if( action == Action::SHOOT_OR_SELECT && m_blockInputDuration <= 0 ) {
        m_nextScreen = std::unique_ptr<Screen>(new MainMenuScreen());
    }
}

//////////////////////////////////////////////////////////////////////
void GameLostScreen::update( float timeDelta )
{ m_blockInputDuration -= timeDelta; }

//////////////////////////////////////////////////////////////////////
std::unique_ptr<Screen> GameLostScreen::nextScreen()
{ return std::move(m_nextScreen); }

//////////////////////////////////////////////////////////////////////
void GameLostScreen::applyAction( ScreenAction& a )
{ a.applyTo(*this); }

} // end ns


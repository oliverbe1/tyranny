#ifndef INC_UNSTABLEPROTECTOR_H
#define INC_UNSTABLEPROTECTOR_H
#include <memory>
#include "BulletFactory.h"
#include "EnemyShip.h"

namespace ty {
class GameObjectAction;

//////////////////////////////////////////////////////////////////////
/// Enemy ship which explodes in a rain of bullets when its shields 
/// are destroyed. Does not fire otherwise. Mainly used as a 
/// protective barrier for more pro-active ships.
//////////////////////////////////////////////////////////////////////
class UnstableProtector : public EnemyShip
{
public:
    UnstableProtector( std::unique_ptr<Script> script );
    virtual void applyAction( GameObjectAction& a ) override;
protected:
    virtual void shoot( BulletFactory& factory ) override;
};

} // end ns

#endif


#ifndef INC_SPIRALSLINGER_H
#define INC_SPIRALSLINGER_H
#include <memory>
#include "BulletFactory.h"
#include "EnemyShip.h"

namespace ty {
class GameObjectAction;

//////////////////////////////////////////////////////////////////////
/// Enemy ship shooting out a rotating spiral of bullets continuously.
//////////////////////////////////////////////////////////////////////
class SpiralSlinger : public EnemyShip
{
public:
    SpiralSlinger( std::unique_ptr<Script> script );
    virtual void applyAction( GameObjectAction& a ) override;
protected:
    virtual void shoot( BulletFactory& factory ) override;
};

} // end ns

#endif


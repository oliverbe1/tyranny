#include <memory>
#include "game/Action.h"
#include "GameFinishedScreen.h"
#include "MainMenuScreen.h"
#include "Screen.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
GameFinishedScreen::GameFinishedScreen() 
: m_blockInputDuration(2.5)
, m_nextScreen(nullptr)
{ }

//////////////////////////////////////////////////////////////////////
void GameFinishedScreen::processAction( const Action& action )
{
    if( action == Action::SHOOT_OR_SELECT && m_blockInputDuration <= 0 ) {
        m_nextScreen = std::unique_ptr<Screen>(new MainMenuScreen());
    }
}

//////////////////////////////////////////////////////////////////////
void GameFinishedScreen::update( float timeDelta )
{
    m_blockInputDuration -= timeDelta;
}

//////////////////////////////////////////////////////////////////////
std::unique_ptr<Screen> GameFinishedScreen::nextScreen()
{
    return std::move(m_nextScreen);
}

//////////////////////////////////////////////////////////////////////
void GameFinishedScreen::applyAction( ScreenAction& a )
{
    a.applyTo(*this);
}

} // end ns

#ifndef INC_SCOUT_H
#define INC_SCOUT_H
#include <memory>
#include "BulletFactory.h"
#include "EnemyShip.h"

namespace ty {
class GameObjectAction;

//////////////////////////////////////////////////////////////////////
/// Small & quick enemy ship. Fires small bullets straight forward.
//////////////////////////////////////////////////////////////////////
class Scout : public EnemyShip
{
public:
    Scout( std::unique_ptr<Script> script );
    virtual void applyAction( GameObjectAction& a ) override;
protected:
    virtual void shoot( BulletFactory& factory ) override;
};

} // end ns

#endif

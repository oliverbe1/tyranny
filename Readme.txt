Building Tyranny
----------------
To build and play the game:
    ./run.sh
Alternatively, you can call cmake directly.

The Tyranny World Format
------------------------
Info on how to tweak or add tyranny levels is under src/resources/Readme.txt.

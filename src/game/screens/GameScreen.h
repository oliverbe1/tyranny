#ifndef INC_GAMESCREEN_H
#define INC_GAMESCREEN_H
#include <memory>
#include "game/Action.h"
#include "game/world/World.h"
#include "Screen.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
/// The in-game screen.
///
/// Displays the current world and any additional GUI elements that 
/// may go with it. Loads in a new world when the current one is finished.
//////////////////////////////////////////////////////////////////////
class GameScreen : public Screen
{
    int                m_worldId;
    unique_ptr<World>  m_world;
    unique_ptr<Screen> m_nextScreen;
    float              m_respawnCooldown;
    int                m_remainingLives;

public:
    //////////////////////////////////////////////////////////////////
    /// Creates an in-game screen for the given world.
    //////////////////////////////////////////////////////////////////
    GameScreen( int worldId );

    //////////////////////////////////////////////////////////////////
    /// Returns the currently loaded world.
    //////////////////////////////////////////////////////////////////
    const World& getWorld() const;

    //////////////////////////////////////////////////////////////////
    /// Returns the number of lives remaining.
    //////////////////////////////////////////////////////////////////
    int getRemainingLives() const;

    // Screen implementation
    void processAction( const Action& action ) override;
    void update( float timeDelta ) override;
    std::unique_ptr<Screen> nextScreen() override;
    void applyAction( ScreenAction& a ) override;

private:
    bool worldExists( int worldId ) const;
    void loadWorld( int worldId, int wave = 1 );
};

} // end ns

#endif


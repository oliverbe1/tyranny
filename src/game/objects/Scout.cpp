#include <cmath>
#include <memory>
#include "Bullet.h"
#include "BulletFactory.h"
#include "EnemyShip.h"
#include "Scout.h"
#include "Script.h"

namespace ty {
using namespace std;

//////////////////////////////////////////////////////////////////////
Scout::Scout( unique_ptr<Script> script )
: EnemyShip( 2.5,   // base speed
             40,    // hit points
             0.8,   // weapon cooldown
             0,     // shooting angle
             0.39,  // radius
             std::move(script) )
{ }

//////////////////////////////////////////////////////////////////////
void Scout::applyAction( GameObjectAction& a )
{ a.applyTo(*this); }

//////////////////////////////////////////////////////////////////////
void Scout::shoot( BulletFactory& factory )
{
    Vector2f pos = getPosition() + Vector2f(0, -getRadius()*1.1);
    Vector2f vel = Vector2f(0,-3.20f);
    factory.spawnHostileBullet(pos, vel, Bullet::TEAR, 0.10);
}

} // end ns


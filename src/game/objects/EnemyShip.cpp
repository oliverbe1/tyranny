#include <iostream>
#include <algorithm>
#include <memory>
#include <cassert>
#include "BulletFactory.h"
#include "EnemyShip.h"
#include "GameObject.h"
#include "Script.h"

namespace ty {
using namespace std;

//////////////////////////////////////////////////////////////////////
EnemyShip::EnemyShip( float baseSpeed, float hitPoints, float cooldown,
                      float shootingAngle, float radius,
                      unique_ptr<Script> script )
: GameObject      (radius)
, m_baseSpeed     (baseSpeed)
, m_hitPoints     (hitPoints)
, m_cooldown      (cooldown)
, m_cooldownRemaining(cooldown)
, m_shootingAngle (shootingAngle)
, m_shooting      (true)
, m_script        (std::move(script))
{ 
    assert(m_script != nullptr); 
}

//////////////////////////////////////////////////////////////////////
float EnemyShip::getBaseSpeed() const
{ return m_baseSpeed; }

//////////////////////////////////////////////////////////////////////
float EnemyShip::getHitPoints() const
{ return m_hitPoints; }

void EnemyShip::damage( float amount ) 
{ m_hitPoints = max(0.f, m_hitPoints - amount); }

//////////////////////////////////////////////////////////////////////
void EnemyShip::setShootingAngle( float shootingAngle )
{ m_shootingAngle = shootingAngle; }

float EnemyShip::getShootingAngle() const
{ return m_shootingAngle; }

void EnemyShip::startShooting()
{ m_shooting = true; }

void EnemyShip::stopShooting()
{ m_shooting = false; }

bool EnemyShip::isShooting() const
{ return m_shooting; }

//////////////////////////////////////////////////////////////////////
void EnemyShip::update( float timeDelta, BulletFactory& factory ) 
{ 
    //cout << "x: " << getPosition().x << "y: " << getPosition().y << endl;
    // Run script. If it's finished destroy this enemy.
    if( m_script->isFinished() ) {
        m_hitPoints = 0;
        return;
    } else {
        m_script->execute(*this, timeDelta);
    }
    // Shoot
    if( isShooting() && m_cooldownRemaining <= 0 ) { 
        shoot(factory); 
        m_cooldownRemaining = m_cooldown;
    } else {
        m_cooldownRemaining -= timeDelta;
    }
    // Move ship
    GameObject::update(timeDelta, factory);
}

} // end ns

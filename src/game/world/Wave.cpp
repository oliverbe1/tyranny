#include <iostream>
#include <memory>
#include <list>
#include "game/objects/EnemyShip.h"
#include "game/objects/Powerup.h"
#include "Wave.h"

namespace ty {
using namespace std;

//////////////////////////////////////////////////////////////////////
Wave::Wave() : m_accumulatedTime(0) { }

//////////////////////////////////////////////////////////////////////
void Wave::wait( float time )
{ m_accumulatedTime += time; }

//////////////////////////////////////////////////////////////////////
void Wave::addShip( unique_ptr<EnemyShip> ship )
{
    m_ships.push_back(make_pair(m_accumulatedTime, std::move(ship)));
    m_order.push_back(SHIP);
    m_accumulatedTime = 0;
}

//////////////////////////////////////////////////////////////////////
void Wave::addPowerup( unique_ptr<Powerup> powerup )
{
    m_powerups.push_back(make_pair(m_accumulatedTime, std::move(powerup)));
    m_order.push_back(POWERUP);
    m_accumulatedTime = 0;
}

//////////////////////////////////////////////////////////////////////
bool Wave::empty() const
{ return m_order.empty(); }

//////////////////////////////////////////////////////////////////////
void Wave::update( float timeDelta )
{
    if( !empty() ) {
        if( m_order.front() == SHIP ) {
            float delay = m_ships.front().first;
            m_ships.front().first = max(delay - timeDelta, 0.f);
        } else
        if( m_order.front() == POWERUP ) {
            float delay = m_powerups.front().first;
            m_powerups.front().first = max(delay - timeDelta, 0.f);
        }
    }
}

//////////////////////////////////////////////////////////////////////
unique_ptr<EnemyShip> Wave::removeReadyShip()
{ 
    if( !empty() && m_order.front() == SHIP && m_ships.front().first <= 0 ) {
        auto p = std::move(m_ships.front());
        m_ships.pop_front();
        m_order.pop_front();
        return std::move(p.second);
    } else {
        return nullptr;
    }
}

//////////////////////////////////////////////////////////////////////
unique_ptr<Powerup> Wave::removeReadyPowerup()
{ 
    if( !empty() && m_order.front() == POWERUP && m_powerups.front().first <= 0 ) {
        auto p = std::move(m_powerups.front());
        m_powerups.pop_front();
        m_order.pop_front();
        return std::move(p.second);
    } else {
        return nullptr;
    }
}


} // end ns

#include <algorithm>
#include <iostream>
#include <memory>
#include <vector>
#include "math/Vector.h"
#include "game/objects/GameObject.h"
#include "game/objects/Bullet.h"
#include "game/objects/BulletFactory.h"
#include "game/objects/Powerup.h"
#include "game/objects/PlayerShip.h"
#include "game/objects/Effect.h"
#include "game/GlobalSettings.h"
#include "Wave.h"
#include "World.h"

namespace ty {
using namespace std;

//////////////////////////////////////////////////////////////////////
const float     World::kBgScrollSpeed (1);                  // tiles/sec
const Vector2f  World::kStartPos      (4.5, 1);
const Rectangle World::kBounds        (-0.5, 7.5, 10., 8.); // bullet bounds
const float     World::kDefaultTimeMultiplier = 1.05;
const float     World::kHarderTimeMultiplier  = 1.38;

//////////////////////////////////////////////////////////////////////
World::World( Background bg, vector<Wave> waves )
: m_bg(bg)
, m_bgPosition(0)
, m_waves(std::move(waves))
, m_currentWave(0)
, m_slowmoFactor(1)
, m_slowmoDuration(0)
{ 
    m_player = unique_ptr<PlayerShip>(new PlayerShip());
    m_player->setPosition(kStartPos);
}

//////////////////////////////////////////////////////////////////////
const Background& World::getBackground() const
{ return m_bg; }

//////////////////////////////////////////////////////////////////////
float World::getBackgroundPosition() const
{ return m_bgPosition; }

//////////////////////////////////////////////////////////////////////
vector<GameObject*> World::getObjects() const
{ 
    vector<GameObject*> objects;
    for( auto& p : m_powerups ) { GameObject* pgo = p.get(); objects.push_back(pgo); }
    for( auto& p : m_enemies  ) { GameObject* pgo = p.get(); objects.push_back(pgo); }
    for( auto& p : m_bullets  ) { GameObject* pgo = p.get(); objects.push_back(pgo); }
    GameObject* player = m_player.get(); objects.push_back(player);
    for( auto& p : m_effects  ) { GameObject* pgo = p.get(); objects.push_back(pgo); }

    return objects;
}

//////////////////////////////////////////////////////////////////////
PlayerShip* World::getPlayer() const
{ return m_player.get(); }

//////////////////////////////////////////////////////////////////////
void World::slowTime( float factor, float duration )
{ m_slowmoFactor = factor; m_slowmoDuration = duration; }

//////////////////////////////////////////////////////////////////////
int World::getCurrentWave() const
{ return (int)m_currentWave + 1; }

//////////////////////////////////////////////////////////////////////
void World::skipWave()
{
    m_enemies.clear();
    m_powerups.clear();
    m_bullets.clear();
    m_effects.clear();
    m_currentWave++;
}

//////////////////////////////////////////////////////////////////////
bool World::isCompleted() const
{ return m_currentWave >= m_waves.size() 
         && m_enemies.empty()
         && !m_player->isDestroyed(); }

//////////////////////////////////////////////////////////////////////
void World::update( float timeDelta )
{
    // Convert real-time to world-time
    timeDelta = convertToWorldTime(timeDelta);

    // Scroll background
    m_bgPosition += timeDelta * kBgScrollSpeed;
    if( m_bgPosition >= m_bg.size() ) { m_bgPosition = 0; }

    // Spawn units from current wave
    if( m_currentWave < m_waves.size() ) {
        Wave& wave = m_waves[m_currentWave];
        wave.update(timeDelta);
        unique_ptr<EnemyShip> ship    = wave.removeReadyShip();
        unique_ptr<Powerup>   powerup = wave.removeReadyPowerup();
        while( ship || powerup ) {
            if( ship    ) { m_enemies.push_back(std::move(ship)); }
            if( powerup ) { m_powerups.push_back(std::move(powerup)); }
            ship    = wave.removeReadyShip();
            powerup = wave.removeReadyPowerup();
        }
        if( wave.empty() && m_enemies.empty() ) { m_currentWave++; }
    }

    // Update individual objects
    BulletFactory factory(*this);
    for( auto pObj : getObjects() ) {
        pObj->update(timeDelta, factory);
    }

    // Deal with collisions
    checkCollisions();

    // Remove objects that are no longer needed
    pruneObjects();
}

// Private
//////////////////////////////////////////////////////////////////////
float World::convertToWorldTime( float timeDelta )
{
    m_slowmoDuration = std::max(0.f, m_slowmoDuration - timeDelta);
    // The base world speed is determined by game difficulty
    if( globalSettings().getDifficulty() == GlobalSettings::Difficulty::HARD ) {
        timeDelta *= kDefaultTimeMultiplier;
    } else {
        timeDelta *= kHarderTimeMultiplier;
    }
    // It may be altered further by e.g. powerups
    if( m_slowmoDuration > 0 && m_slowmoDuration < 1.55 ) {
        timeDelta += ((timeDelta * m_slowmoFactor) - timeDelta) * (m_slowmoDuration / 1.55);
    } else
    if( m_slowmoDuration > 0 ) {
        timeDelta *= m_slowmoFactor;
    }
    return timeDelta;
}

//////////////////////////////////////////////////////////////////////
// Swaps the ptr at index `i` with the last item, then erases it.
template <typename T>
static void removePtrAt( vector<unique_ptr<T>>& v, size_t i ) {
    v[i] = std::move(v.back());
    v.pop_back();
}

//////////////////////////////////////////////////////////////////////
void World::checkCollisions()
{
    bool playerIsVulnerable = !m_player->isDestroyed() && !globalSettings().godModeEnabled();

    // Powerups<->Player
    for( size_t i = 0; i < m_powerups.size(); ++i ) {
        auto& powerup = m_powerups[i];
        if( m_player->intersects(*powerup) ) {
            powerup->onPickup(*this);
            removePtrAt(m_powerups, i); // remove powerup
            i--;
        }
    }
    // Bullets
    for( size_t i = 0; i < m_bullets.size(); ++i ) {
        auto& bullet = m_bullets[i];
        // Bullet<->Player
        if( m_player->intersects(*bullet) 
                && !bullet->isFriendly() && playerIsVulnerable ) {
            addEffect(Effect::EXPLOSION, m_player->getPosition());
            m_player->destroy();
            removePtrAt(m_bullets, i); // remove bullet
            i--; continue;
        }
        // Bullet<->Enemy
        for( size_t j = 0; j < m_enemies.size(); ++j ) {
            auto& enemy = m_enemies[j];
            if( enemy->intersects(*bullet) && bullet->isFriendly() ) {
                enemy->damage(bullet->getDamage());
                removePtrAt(m_bullets, i); // remove bullet
                i--; break;
            }
        }
    }
    // Enemies<->Player
    for( size_t i = 0; i < m_enemies.size(); ++i ) {
        auto& enemy = m_enemies[i];
        // Remove enemy
        if( enemy->getHitPoints() <= 0 ) {
            addEffect(Effect::EXPLOSION, enemy->getPosition());
            removePtrAt(m_enemies, i); 
            i--;
            continue;
        }
        if( m_player->intersects(*enemy) && playerIsVulnerable ) {
            addEffect(Effect::EXPLOSION, m_player->getPosition());
            m_player->destroy();
            continue;
        }
    }
}

//////////////////////////////////////////////////////////////////////
void World::addEffect( Effect::Graphic g, Vector2f pos )
{
    Effect* effect = new Effect(g);
    effect->setPosition(pos);
    m_effects.push_back(unique_ptr<Effect>(effect));
}

//////////////////////////////////////////////////////////////////////
void World::pruneObjects()
{
    // Prune out of bounds bullets
    {
        auto it = remove_if( m_bullets.begin(), m_bullets.end(), 
                  [](unique_ptr<Bullet>& b) { return !kBounds.inBounds(b->getPosition()); } );
        m_bullets.erase(it, m_bullets.end());
    }
    // Prune out of bounds powerups
    {
        auto it = remove_if( m_powerups.begin(), m_powerups.end(), 
                  [](unique_ptr<Powerup>& p) { return p->getPosition().y < -5; } );
        m_powerups.erase(it, m_powerups.end());
    }
    // Prune completed effects
    {
        auto it = remove_if( m_effects.begin(), m_effects.end(), 
                  [](unique_ptr<Effect>& e) { return e->getRemaining() <= 0; } );
        m_effects.erase(it, m_effects.end());
    }
}


} // end ns

#ifndef INC_ENEMYSHIP_H
#define INC_ENEMYSHIP_H
#include "GameObject.h"
#include "Script.h"

namespace ty {
class BulletFactory;

//////////////////////////////////////////////////////////////////////
/// Abstract base class for enemy ships.
//////////////////////////////////////////////////////////////////////
class EnemyShip : public GameObject
{
    float m_baseSpeed;
    float m_hitPoints;
    float m_cooldown;
    float m_cooldownRemaining;
    float m_shootingAngle;
    bool  m_shooting;
    std::unique_ptr<Script> m_script;

public:
    //////////////////////////////////////////////////////////////////
    /// Constructs a new Enemy Ship.
    ///
    /// \param baseSpeed the default speed the ship should move at when 
    ///                  instructed by a script. In game units/sec.
    /// \param hitPoints how much damage this ship can take.
    /// \param cooldown how long ship's weapon takes to recharge, in seconds.
    /// \param shootingAngle angle shots should be fired at. 0° == downwards.
    ///                      This may be modified during script execution.
    /// \param radius body size, ship will take damage in this radius.
    /// \param script the set of moves this ship will execute when 
    ///               update() is called.
    //////////////////////////////////////////////////////////////////
    EnemyShip( float baseSpeed, float hitPoints, float cooldown,
               float shootingAngle, float radius,
               std::unique_ptr<Script> script );
    virtual ~EnemyShip() { }

    float getBaseSpeed() const;
    float getHitPoints() const;
    void  damage( float amount );

    void  setShootingAngle( float angle );
    float getShootingAngle() const;
    void  startShooting();
    void  stopShooting();
    bool  isShooting() const;

    virtual void update( float timeDelta, BulletFactory& factory ) override;

protected:
    //////////////////////////////////////////////////////////////////
    /// All subclassed enemy ships should define their unique shooting 
    /// action. This method will be automatically invoked during update()
    /// whenever the ship's weapon is ready, and the ship is currently 
    /// shooting (which is by default).
    //////////////////////////////////////////////////////////////////
    virtual void shoot( BulletFactory& factory ) = 0;
};

} //end ns

#endif

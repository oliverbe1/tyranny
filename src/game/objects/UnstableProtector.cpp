#include <cmath>
#include <memory>
#include "math/algorithm.h"
#include "BulletFactory.h"
#include "UnstableProtector.h"
#include "Script.h"

namespace ty {
using namespace std;
using namespace ty::math;

//////////////////////////////////////////////////////////////////////
UnstableProtector::UnstableProtector( unique_ptr<Script> script )
: EnemyShip( 2,    // base speed
             1020,  // hit points
             0,  // weapon cooldown
             0,    // shooting angle
             0.25,  // radius
             std::move(script) )
{ }

//////////////////////////////////////////////////////////////////////
void UnstableProtector::applyAction( GameObjectAction& a )
{ a.applyTo(*this); }

//////////////////////////////////////////////////////////////////////
void UnstableProtector::shoot( BulletFactory& factory )
{
    static const float pi = 3.14159265359;
    // Shoot when shields are broken
    if( getHitPoints() <= 1000 ) {
        float offset = randfloat();
        for( float i = offset; i < 1+offset; i += 0.077 ) {
            float rad = i * 2*pi;
            float px  = std::cos(rad) * getRadius();
            float py  = std::sin(rad) * getRadius();
            Vector2f pos = getPosition() + Vector2f(px, py);
            Vector2f vel = Vector2f(8*px, 8*py);
            factory.spawnHostileBullet(pos, vel, Bullet::BALL, 0.09);
        }
        damage(1000);
    }
}

} // end ns


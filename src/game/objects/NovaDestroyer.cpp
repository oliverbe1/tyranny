#include <cmath>
#include <memory>
#include "BulletFactory.h"
#include "NovaDestroyer.h"
#include "Script.h"

namespace ty {
using namespace std;

//////////////////////////////////////////////////////////////////////
NovaDestroyer::NovaDestroyer( unique_ptr<Script> script )
: EnemyShip( 0.7,    // base speed
             150,  // hit points
             1.5,  // weapon cooldown
             0,    // shooting angle
             0.45,  // radius
             std::move(script) )
{ }

//////////////////////////////////////////////////////////////////////
void NovaDestroyer::applyAction( GameObjectAction& a )
{ a.applyTo(*this); }

//////////////////////////////////////////////////////////////////////
void NovaDestroyer::shoot( BulletFactory& factory )
{
    static const float pi = 3.14159265359;
    for( float i = 0; i < 1; i += 0.077 ) {
        float px = std::cos(i*2*pi) * getRadius();
        float py = std::sin(i*2*pi) * getRadius();
        Vector2f pos = getPosition() + Vector2f(px, py);
        Vector2f vel = Vector2f(4*px, 4*py);
        factory.spawnHostileBullet(pos, vel, Bullet::HEART, 0.15);
    }
}

} // end ns

#ifndef INC_PLAYERSHIP_H
#define INC_PLAYERSHIP_H
#include "math/Vector.h"
#include "math/Rectangle.h"
#include "GameObject.h"
#include "Bullet.h"
#include "BulletFactory.h"

namespace ty {

enum class Weapon { STANDARD, SCATTERSHOT, YCOMBINATOR };

//////////////////////////////////////////////////////////////////////
/// The ship under player control.
//////////////////////////////////////////////////////////////////////
class PlayerShip : public GameObject
{
    const static float kRadius;
    const static float kHorizontalAcc;
    const static float kVerticalAcc;
    const static float kHorizontalDeAcc;
    const static float kVerticalDeAcc;
    const static float kMaxHorizontalVel;
    const static float kMaxVerticalVel;
    const static float kWeaponStandardCD;
    const static float kWeaponScatterCD;
    const static float kWeaponYCombCD;
    const static Rectangle kMovementBounds;

    Vector2f m_acc;

    Weapon m_weapon;
    float m_cooldown;
    float m_fire;

    bool m_destroyed;

public:
    PlayerShip();
    PlayerShip( Vector2f pos, Vector2f vel );

    //////////////////////////////////////////////////////////////////
    /// Apply acceleration to the left. Note that no change in movement 
    /// occurs until `update` is called.
    //////////////////////////////////////////////////////////////////
    void moveLeft();
    //////////////////////////////////////////////////////////////////
    /// Apply acceleration to the right. Note that no change in movement 
    /// occurs until `update` is called.
    //////////////////////////////////////////////////////////////////
    void moveRight();
    //////////////////////////////////////////////////////////////////
    /// Apply upward acceleration. Note that no change in movement 
    /// occurs until `update` is called.
    //////////////////////////////////////////////////////////////////
    void moveUp();
    //////////////////////////////////////////////////////////////////
    /// Apply downward acceleration. Note that no change in movement 
    /// occurs until `update` is called.
    //////////////////////////////////////////////////////////////////
    void moveDown();

    //////////////////////////////////////////////////////////////////
    /// Fires the ship's weapon during the next `update`, if it is not 
    /// on cooldown.
    //////////////////////////////////////////////////////////////////
    void shoot();
    //////////////////////////////////////////////////////////////////
    /// Changes the ship's weapon. Weapons influence what kind of bullets 
    /// are fired when `shoot()` is called.
    //////////////////////////////////////////////////////////////////
    void changeWeapon( Weapon weapon );

    //////////////////////////////////////////////////////////////////
    /// Destroys the ship.
    //////////////////////////////////////////////////////////////////
    void destroy();
    //////////////////////////////////////////////////////////////////
    /// Returns whether the ship has been destroyed.
    //////////////////////////////////////////////////////////////////
    bool isDestroyed() const;

    //////////////////////////////////////////////////////////////////
    /// Advances the state of the ship by `timeDelta` seconds.
    //////////////////////////////////////////////////////////////////
    virtual void update( float timeDelta, BulletFactory& factory ) override;

    virtual void applyAction( GameObjectAction& a ) override;

private:
    void updateMovement( float timeDelta, BulletFactory& factory );
    void fireWeapons( float timeDelta, BulletFactory& factory );
    void checkBounds();
};

} // end ns

#endif

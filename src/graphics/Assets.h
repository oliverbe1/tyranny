#ifndef INC_ASSETS_H
#define INC_ASSETS_H
#include <SFML/Graphics.hpp>
#include <string>

namespace ty {
namespace sfml {
class Assets;

//////////////////////////////////////////////////////////////////////
/// Returns a reference to the assets singleton.
//////////////////////////////////////////////////////////////////////
Assets& assets();

//////////////////////////////////////////////////////////////////////
/// Stores every asset (sprites and such) used by the game.
//////////////////////////////////////////////////////////////////////
class Assets 
{
public:
    // Bullets
    sf::Sprite BULLET_BALL;
    sf::Sprite BULLET_BOMB;
    sf::Sprite BULLET_HEART;
    sf::Sprite BULLET_PLAYER;
    sf::Sprite BULLET_TEAR;
    sf::Sprite BULLET_TOXIC;

    // Player
    sf::Sprite PLAYER;

    // Enemies
    sf::Sprite ENEMY_MADBOMBER;
    sf::Sprite ENEMY_NOVADESTROYER;
    sf::Sprite ENEMY_SCOUT;;
    sf::Sprite ENEMY_SPIRALSLINGER;
    sf::Sprite ENEMY_UNSTABLEPROTECTOR;

    // Powerups
    sf::Sprite POWERUP_SCATTERSHOT;
    sf::Sprite POWERUP_YCOMBINATOR;
    sf::Sprite POWERUP_SLOWMO;

    // Effects
    sf::Sprite EFFECT_EXPLOSION;

    // Tiles
    sf::Sprite TILE_BROWN;
    sf::Sprite TILE_BROWN_SLANTED;
    sf::Sprite TILE_PLATE_BLUE;
    sf::Sprite TILE_PLATE_GREEN;
    sf::Sprite TILE_SQUARES;

    // Menu / GUI
    sf::Sprite MENU_MAIN_HARD;
    sf::Sprite MENU_MAIN_HARDER;
    sf::Sprite MENU_WON;
    sf::Sprite MENU_LOST;
    sf::Sprite GUI_LIVES_0;
    sf::Sprite GUI_LIVES_1;
    sf::Sprite GUI_LIVES_2;
    sf::Sprite GUI_LIVES_3;

    friend Assets& assets();
private:
    Assets();

    sf::Texture loadTexture( const std::string& name ) const;

    // TEXTURES
    // Bullets
    sf::Texture T_BULLET_BALL;
    sf::Texture T_BULLET_BOMB;
    sf::Texture T_BULLET_HEART;
    sf::Texture T_BULLET_PLAYER;
    sf::Texture T_BULLET_TEAR;
    sf::Texture T_BULLET_TOXIC;

    // Player
    sf::Texture T_PLAYER;

    // Enemies
    sf::Texture T_ENEMY_MADBOMBER;
    sf::Texture T_ENEMY_NOVADESTROYER;
    sf::Texture T_ENEMY_SCOUT;;
    sf::Texture T_ENEMY_SPIRALSLINGER;
    sf::Texture T_ENEMY_UNSTABLEPROTECTOR;

    // Powerups
    sf::Texture T_POWERUP_SCATTERSHOT;
    sf::Texture T_POWERUP_YCOMBINATOR;
    sf::Texture T_POWERUP_SLOWMO;

    // Effects
    sf::Texture T_EFFECT_EXPLOSION;

    // Tiles
    sf::Texture T_TILE_BROWN;
    sf::Texture T_TILE_BROWN_SLANTED;
    sf::Texture T_TILE_PLATE_BLUE;
    sf::Texture T_TILE_PLATE_GREEN;
    sf::Texture T_TILE_SQUARES;

    // Menu
    sf::Texture T_MENU_MAIN_HARD;
    sf::Texture T_MENU_MAIN_HARDER;
    sf::Texture T_MENU_WON;
    sf::Texture T_MENU_LOST;
    sf::Texture T_GUI_LIVES_0;
    sf::Texture T_GUI_LIVES_1;
    sf::Texture T_GUI_LIVES_2;
    sf::Texture T_GUI_LIVES_3;
};

} // end ns sfml
} // end ns ty
#endif

#ifndef INC_GAMEOBJECT_H
#define INC_GAMEOBJECT_H
#include "math/Vector.h"

namespace ty {
class GameObjectAction;
class BulletFactory;

//////////////////////////////////////////////////////////////////////
/// Abstract base class for dynamic game objects.
///
/// All objects have a position, velocity and collision radius.
/// One in-game unit length corresponds to the length of a tile.
//////////////////////////////////////////////////////////////////////
class GameObject
{
private:
    Vector2f m_pos;
    Vector2f m_vel;
    float    m_radius;

public:
    //////////////////////////////////////////////////////////////////
    /// Initializes base game object with radius.
    /// Position and velocity are initialized to zero.
    //////////////////////////////////////////////////////////////////
    GameObject( float radius );

    virtual ~GameObject() { }

    //////////////////////////////////////////////////////////////////
    /// Initializes base game object with initial position, velocity 
    /// and radius.
    //////////////////////////////////////////////////////////////////
    GameObject( Vector2f pos, Vector2f vel, float radius );

    Vector2f     getPosition() const;         ///< Get object's position
    void         setPosition( Vector2f pos ); ///< Set object's position
    Vector2f     getVelocity() const;         ///< Get object's velocity
    void         setVelocity( Vector2f vel ); ///< Set object's velocity
    float        getRadius() const;           ///< Get object's radius

    //////////////////////////////////////////////////////////////////
    /// Determines whether two objects intersect based on radius.
    //////////////////////////////////////////////////////////////////
    bool         intersects( const GameObject& other ) const;

    //////////////////////////////////////////////////////////////////
    /// Advances the state of the game object by `timeDelta` seconds.
    /// Unless overridden, advances the object's position based on 
    /// its current velocity.
    //////////////////////////////////////////////////////////////////
    virtual void update( float timeDelta, BulletFactory& factory );

    //////////////////////////////////////////////////////////////////
    /// Applies the given action/visitor to this game object.
    //////////////////////////////////////////////////////////////////
    virtual void applyAction( GameObjectAction& a ) = 0;
};

//////////////////////////////////////////////////////////////////////
/// Interface specifying a visitor/action on game objects.
///
/// Allows non-member methods to dispatch on the dynamic type 
/// of game objects.
//////////////////////////////////////////////////////////////////////
class PlayerShip;
class NovaDestroyer;
class UnstableProtector;
class SpiralSlinger;
class Scout;
class MadBomber;
class Bullet;
class WeaponPowerup;
class SlowmoPowerup;
class Effect;

class GameObjectAction
{
public:
    // Player
    virtual void applyTo( PlayerShip& ship ) = 0;
    // Enemy ships
    virtual void applyTo( NovaDestroyer& ship ) = 0;
    virtual void applyTo( UnstableProtector& ship ) = 0;
    virtual void applyTo( SpiralSlinger& ship ) = 0;
    virtual void applyTo( Scout& ship ) = 0;
    virtual void applyTo( MadBomber& ship ) = 0;
    // Bullets
    virtual void applyTo( Bullet& bullet ) = 0;
    // Powerups
    virtual void applyTo( WeaponPowerup& powerup ) = 0;
    virtual void applyTo( SlowmoPowerup& powerup ) = 0;
    // Effects
    virtual void applyTo( Effect& effect ) = 0;

};

} // end ns

#endif

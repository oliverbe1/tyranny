#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <cctype>
#include <string>
#include <vector>
#include <map>

#include "math/Vector.h"
#include "game/objects/PlayerShip.h"
#include "game/objects/NovaDestroyer.h"
#include "game/objects/UnstableProtector.h"
#include "game/objects/SpiralSlinger.h"
#include "game/objects/Scout.h"
#include "game/objects/MadBomber.h"
#include "game/objects/Script.h"

#include "Tile.h"
#include "Wave.h"
#include "World.h"
#include "WorldParser.h"

namespace ty {
using namespace std;

//////////////////////////////////////////////////////////////////////
//
//                   Barebones S-Expression parser
//
//////////////////////////////////////////////////////////////////////
struct SExpression
{
    enum Type { List, Symbol, Number };
    Type   type;
    string value;
    float  numValue;
    vector<SExpression> children;
};
static SExpression List  ( const vector<SExpression>& children )
{ return SExpression{SExpression::List, "", 0.0, children}; }
static SExpression Symbol( const string& value )
{ return SExpression{SExpression::Symbol, value, 0.0, {}}; }
static SExpression Number( float numValue )
{ return SExpression{SExpression::Number, "", numValue, {}}; }

static SExpression readSExpression( istream& is );

static bool endOfStream( istream& is ) {
    char ch; 
    if( is >> ch ) { 
        if( ch == ';' ) { // skip comments
            string line; getline(is, line);
            return endOfStream(is);
        } else {
            is.unget();
            return false; 
        }
    }
    return true;
}

static SExpression readList( istream& is ) {
    vector<SExpression> children;
    is.get(); // consume '('
    for( char ch; is >> ch; ) {
        if( ch == ')' ) {
            return List(children);
        } else {
            is.unget();
            children.push_back( readSExpression(is) );
        }
    }
    is.clear(); string rest; is >> rest;
    throw runtime_error("failure to read list in world file near " + rest);
}

static SExpression readSymbol( istream& is ) {
    string s;
    for( char ch; is.get(ch) && (isalnum(ch) || ch == '-'); ) {
        s += ch;
    }
    if( is ) { is.unget(); }
    return Symbol(s);
}

static SExpression readSExpression( istream& is ) {
    char ch; is >> ch;
    if( !is ) { throw runtime_error("expected expression reading world file"); }
    is.unget();

    if( ch == ';' ) { // skip comments
        string line; getline(is, line);
        return readSExpression(is);
    } else
    if( ch == '(' ) {
        return readList(is);
    } else
    if( isdigit(ch) || ch == '-' ) {
        float nr; is >> nr;
        return Number(nr);
    } else 
    if( isalpha(ch) ) {
        return readSymbol(is);
    } else {
        string rest; is >> rest;
        throw runtime_error("unexpected character reading world file: " + rest); 
    }
}

//////////////////////////////////////////////////////////////////////
//
//                S-Expressions to World conversions
//
//////////////////////////////////////////////////////////////////////
typedef map<string, unique_ptr<Script>> ScriptMap;

// Returns the name of the first symbol in the list, if any.
static string header( SExpression& exp ) { 
    if( exp.type == SExpression::List && !exp.children.empty() ) {
        return exp.children[0].value;
    }
    return "";
}

// BACKGROUND PARSING
// Reads one row of a background definition in s-exp form
static vector<Tile> readBackgroundRow( SExpression& rowExp ) {
    vector<Tile> row;
    if( rowExp.children.size() < 9 ) {
        throw runtime_error("background row must contain 9 tiles"); }
    for( SExpression& tileExp : rowExp.children ) {
        Tile tile;
        if     ( tileExp.value == "S" ) { tile = Tile::SQUARES; }
        else if( tileExp.value == "P" ) { tile = Tile::PLATE_BLUE; }
        else if( tileExp.value == "p" ) { tile = Tile::PLATE_GREEN; }
        else if( tileExp.value == "B" ) { tile = Tile::BROWN; }
        else if( tileExp.value == "b" ) { tile = Tile::BROWN_SLANTED; }
        else   { throw runtime_error("unrecognized tile " + tileExp.value); }
        row.push_back(tile);
    }
    return row;
}

// Reads the background definition from a (background ..) expression
static Background readBackground( SExpression& bgExp ) {
    if( header(bgExp) != "background" ) {
        throw runtime_error("expected background definition at start of world file"); }
    if( bgExp.children.size() < 9 ) {
        throw runtime_error("background must contain 8+ rows."); }
    Background bg;
    for( size_t i = 1; i < bgExp.children.size(); ++i ) {
        SExpression& rowExp = bgExp.children[i];
        bg.push_back(readBackgroundRow(rowExp));
    }
    std::reverse(bg.begin(), bg.end());
    return bg;
}

// WAVE PARSING
// Reads a (x y) position expression.
static Vector2f readPosition( SExpression& exp ) {
    if( exp.children.size() < 2 ) { throw runtime_error("invalid position"); }
    return Vector2f(exp.children[0].numValue, exp.children[1].numValue);
}

// Reads a (x y s) position + speedmultiplier expression.
static pair<Vector2f,float> readPositionAndSpeed( SExpression& exp ) {
    if( exp.children.size() < 2 ) { throw runtime_error("invalid position"); }
    float speed = exp.children.size() == 2 ? 1 : exp.children[2].numValue;
    return make_pair(
            Vector2f(exp.children[0].numValue, exp.children[1].numValue),
            speed);
}

// Reads a script expression. eg (do (follow-path .. ))
static unique_ptr<Script> 
readScript( SExpression& exp, ScriptMap& scripts ) {
    string scriptName = header(exp);
    if( scriptName == "follow-path" ) {
        if( exp.children.size() < 2 ) { throw runtime_error("invalid follow-path"); }
        vector<Vector2f> relativePath;
        vector<float>    speedMultipliers;
        for( size_t i = 1; i < exp.children.size(); ++i ) {
            auto pair = readPositionAndSpeed(exp.children[i]);
            relativePath.push_back(pair.first);
            speedMultipliers.push_back(pair.second);
        }
        return unique_ptr<Script>(
                new FollowPath(relativePath, speedMultipliers));
    } else
    if( scriptName == "idle" ) {
        if( exp.children.size() != 2 ) { throw runtime_error("invalid idle"); }
        return unique_ptr<Script>(
                new Idle(exp.children[1].numValue));
    } else
    if( scriptName == "start-shooting" ) {
        return unique_ptr<Script>(new StartShooting());
    } else
    if( scriptName == "stop-shooting" ) {
        return unique_ptr<Script>(new StopShooting());
    } else
    if( scriptName == "loop" ) {
        if( exp.children.size() != 3 ) { throw runtime_error("invalid loop"); }
        int numIterations = exp.children[1].numValue;
        return unique_ptr<Script>(
                new Loop(readScript(exp.children[2],scripts), numIterations));
    } else
    if( scriptName == "do" ) {
        Sequence* s = new Sequence();
        for( size_t i = 1; i < exp.children.size(); ++i ) {
            s->addScript( readScript(exp.children[i], scripts) );
        }
        return unique_ptr<Script>(s);
    } else
    if( scriptName == "load-script" ) {
        if( exp.children.size() != 2 ) { throw runtime_error("invalid load-script"); }
        string name = exp.children[1].value; 
        auto it = scripts.find(name);
        if( it != scripts.end() ) {
            return it->second->clone();
        } else {
            throw runtime_error("undefined script " + name);
        }
    } else {
        throw runtime_error("unknown script type " + scriptName);
    }
}

// Stores a script defined in a (script name ...) expression
static void storeScript( SExpression& exp, ScriptMap& scripts ) {
    if( exp.children.size() != 3 ) { throw runtime_error("invalid script expression"); }
    string name = exp.children[1].value;
    scripts[name] = readScript(exp.children[2], scripts);
}

// Creates a script as named in a (spawn ..) expression
static unique_ptr<EnemyShip> createShip( string name, 
        Vector2f pos, unique_ptr<Script> script ) {
    EnemyShip* ship;
    if( name == "NovaDestroyer" ) {
        ship = new NovaDestroyer(std::move(script));
    } else
    if( name == "UnstableProtector" ) {
        ship = new UnstableProtector(std::move(script));
    } else
    if( name == "SpiralSlinger" ) {
        ship = new SpiralSlinger(std::move(script));
    } else
    if( name == "Scout" ) {
        ship = new Scout(std::move(script));
    } else
    if( name == "MadBomber" ) {
        ship = new MadBomber(std::move(script));
    } else {
        throw runtime_error("unknown ship type " + name);
    }
    ship->setPosition(pos);
    return unique_ptr<EnemyShip>(ship);
}

// Creates a powerup as named in a (powerup ..) expression
static unique_ptr<Powerup> createPowerup( string name, Vector2f pos ) {
    Powerup* powerup;
    if( name == "ScatterShot" ) {
        powerup = new WeaponPowerup(Weapon::SCATTERSHOT);
    } else
    if( name == "YCombinator" ) {
        powerup = new WeaponPowerup(Weapon::YCOMBINATOR);
    } else
    if( name == "SlowMotion" ) {
        powerup = new SlowmoPowerup();
    } else {
        throw runtime_error("unknown powerup type " + name);
    }
    powerup->setPosition(pos);
    return unique_ptr<Powerup>(powerup);
}

// Read one wave from a (wave ..) s-expression.
static Wave readWave( SExpression& waveExp, ScriptMap& scripts ) {
    Wave wave;
    for( size_t i = 1; i < waveExp.children.size(); ++i ) {
        SExpression& actionExp = waveExp.children[i];
        if( header(actionExp) == "spawn" ) {
            if( actionExp.children.size() != 4 ) { throw runtime_error("invalid spawn expression"); }
            string   name = actionExp.children[1].value;
            Vector2f pos  = readPosition(actionExp.children[2]);
            unique_ptr<Script> script = readScript(actionExp.children[3], scripts);
            wave.addShip(createShip(name,pos,std::move(script)));
        } else 
        if( header(actionExp) == "powerup" ) {
            if( actionExp.children.size() != 3 ) { throw runtime_error("invalid powerup expression"); }
            string   name = actionExp.children[1].value;
            Vector2f pos  = readPosition(actionExp.children[2]);
            wave.addPowerup(createPowerup(name,pos));
        } else 
        if( header(actionExp) == "wait" ) {
            if( actionExp.children.size() != 2 ) { throw runtime_error("invalid wait expression"); }
            wave.wait(actionExp.children[1].numValue);
        } else {
            throw runtime_error("unknown wave action " + header(actionExp));
        }
    }
    return wave;
}

// Reads all (wave ..) expressions from stream and converts to Wave objects.
static vector<Wave> readWaves( istream& is ) {
    ScriptMap    scripts;  // scripts used by "load-script" actions
    vector<Wave> waves;

    while( !endOfStream(is) ) {
        SExpression exp = readSExpression(is);
        string expName = header(exp);
        if( expName == "script" ) {
            storeScript(exp, scripts);
        } else 
        if( expName == "wave" ) {
            waves.push_back(readWave(exp, scripts));
        } else {
            throw runtime_error("unknown top-level expression " + expName);
        }
    }

    return waves;
}

//////////////////////////////////////////////////////////////////////
//
//                          WorldParser
//
//////////////////////////////////////////////////////////////////////
WorldParser::WorldParser() { }

//////////////////////////////////////////////////////////////////////
World WorldParser::readFromFile( const std::string& name )
{
    ifstream ifs(name);
    if( !ifs ) { throw runtime_error("could not open world file " + name); }
    if( endOfStream(ifs) ) { throw runtime_error("world file is empty " + name); }

    SExpression  bgExp = readSExpression(ifs);
    Background   bg    = readBackground(bgExp);
    vector<Wave> waves = readWaves(ifs);

    return World(bg, std::move(waves));
}

} // end ns

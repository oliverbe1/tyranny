#ifndef INC_SCREEN_H
#define INC_SCREEN_H
#include <memory>
#include "game/Action.h"

namespace ty {
class ScreenAction;

//////////////////////////////////////////////////////////////////////
/// Interface for screens which may appear in the game.
///
/// Examples are menu screens or the in-game screen.
//////////////////////////////////////////////////////////////////////
class Screen
{
public:
    //////////////////////////////////////////////////////////////////
    /// Processes an action. Typically the result of user input.
    //////////////////////////////////////////////////////////////////
    virtual void processAction( const Action& action ) = 0;

    //////////////////////////////////////////////////////////////////
    /// Advances the state of the screen by `timeDelta` seconds.
    //////////////////////////////////////////////////////////////////
    virtual void update( float timeDelta ) = 0;

    //////////////////////////////////////////////////////////////////
    /// Returns which screen should be loaded next.
    /// Equals nullptr if this screen should stay.
    //////////////////////////////////////////////////////////////////
    virtual std::unique_ptr<Screen> nextScreen() = 0;

    //////////////////////////////////////////////////////////////////
    /// Applies the given action/visitor to this screen.
    //////////////////////////////////////////////////////////////////
    virtual void applyAction( ScreenAction& a ) = 0;

    virtual ~Screen() { }
};

class GameScreen;
class MainMenuScreen;
class GameLostScreen;
class GameFinishedScreen;
//////////////////////////////////////////////////////////////////////
/// Interface specifying an action/visitor on screens.
//
/// Allows non-member methods to dispatch on the dynamic type 
/// of screens.
//////////////////////////////////////////////////////////////////////
class ScreenAction
{
public:
    virtual void applyTo( GameScreen& screen ) = 0;
    virtual void applyTo( MainMenuScreen& screen ) = 0;
    virtual void applyTo( GameLostScreen& screen ) = 0;
    virtual void applyTo( GameFinishedScreen& screen ) = 0;
};

} // end ns

#endif

#include <SFML/Graphics.hpp>
#include <vector>
#include "game/Action.h"
#include "InputHandler.h"

namespace ty {
namespace sfml {
using std::vector;

//////////////////////////////////////////////////////////////////////
InputHandler::InputHandler( sf::RenderWindow& window )
: m_window(window) { }

//////////////////////////////////////////////////////////////////////
vector<ty::Action> InputHandler::readInput()
{
    vector<ty::Action> actions;

    // Read game related input
    if( sf::Keyboard::isKeyPressed(sf::Keyboard::Q)
            || sf::Keyboard::isKeyPressed(sf::Keyboard::A)
            || sf::Keyboard::isKeyPressed(sf::Keyboard::Left) ) {
        actions.push_back(ty::Action::LEFT);
    } 
    if( sf::Keyboard::isKeyPressed(sf::Keyboard::Z)
            || sf::Keyboard::isKeyPressed(sf::Keyboard::W)
            || sf::Keyboard::isKeyPressed(sf::Keyboard::Up) ) {
        actions.push_back(ty::Action::UP);
    } 
    if( sf::Keyboard::isKeyPressed(sf::Keyboard::D)
            || sf::Keyboard::isKeyPressed(sf::Keyboard::Right) ) {
        actions.push_back(ty::Action::RIGHT);
    } 
    if( sf::Keyboard::isKeyPressed(sf::Keyboard::S)
            || sf::Keyboard::isKeyPressed(sf::Keyboard::Down) ) {
        actions.push_back(ty::Action::DOWN);
    } 
    if( sf::Keyboard::isKeyPressed(sf::Keyboard::Space) || 
            sf::Keyboard::isKeyPressed(sf::Keyboard::Return) ) {
        actions.push_back(ty::Action::SHOOT_OR_SELECT);
    } 

    // Some keys may have been pressed in between readInput() calls.
    sf::Event event;
    while( m_window.pollEvent(event) )
    {
        if( event.type == sf::Event::Closed ) {
            m_window.close();
            return actions;
        } else
        if( event.type == sf::Event::KeyReleased ) {
            if( event.key.code == sf::Keyboard::Escape ) {
                m_window.close();
            } else
            if( event.key.code == sf::Keyboard::F2 ) {
                actions.push_back(ty::Action::RESTART_LEVEL);
            } else
            if( event.key.code == sf::Keyboard::F3 ) {
                actions.push_back(ty::Action::NEXT_LEVEL);
            } else
            if( event.key.code == sf::Keyboard::F4 ) {
                actions.push_back(ty::Action::TOGGLE_GODMODE);
            } else
            if( event.key.code == sf::Keyboard::F5 ) {
                actions.push_back(ty::Action::SKIP_WAVE);
            } 
        }
    }

    return actions;
}

} // end ns sfml
} // end ns ty

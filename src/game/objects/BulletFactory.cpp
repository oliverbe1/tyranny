#include <memory>
#include "math/Vector.h"
#include "game/world/World.h"
#include "BulletFactory.h"
#include "Bullet.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
BulletFactory::BulletFactory( World& world )
: m_world(world) { }

//////////////////////////////////////////////////////////////////////
void BulletFactory::spawnFriendlyBullet( Vector2f pos, Vector2f vel, 
        Bullet::Graphic g, float damage, float radius )
{
    unique_ptr<Bullet> b( new Bullet(g, true, damage, radius) );
    b->setPosition(pos);
    b->setVelocity(vel);
    m_world.m_bullets.push_back(std::move(b));
}

//////////////////////////////////////////////////////////////////////
void BulletFactory::spawnHostileBullet ( Vector2f pos, Vector2f vel, 
        Bullet::Graphic g, float radius )
{
    unique_ptr<Bullet> b( new Bullet(g, false, 1, radius) );
    b->setPosition(pos);
    b->setVelocity(vel);
    m_world.m_bullets.push_back(std::move(b));
}

} // end ns

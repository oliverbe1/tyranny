#ifndef INC_RENDERSCREEN_H
#define INC_RENDERSCREEN_H
#include <SFML/Graphics.hpp>
#include "game/screens/Screen.h"

namespace ty {
namespace sfml {

//////////////////////////////////////////////////////////////////////
/// Action for rendering screens to a SFML window.
//////////////////////////////////////////////////////////////////////
class RenderScreen : public ScreenAction
{
    sf::RenderWindow& m_window;
    int m_pixelsPerUnit;

public:
    RenderScreen( sf::RenderWindow& window, int pixelsPerUnit );

    virtual void applyTo( ty::GameScreen& screen ) override;
    virtual void applyTo( ty::MainMenuScreen& screen ) override;
    virtual void applyTo( ty::GameLostScreen& screen ) override;
    virtual void applyTo( ty::GameFinishedScreen& screen ) override;
};

} // end ns sfml
} // end ns ty

#endif


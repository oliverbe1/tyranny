# simple script to help generate paths
import math
import sys

""" Convert number in [0,1] range to radians. """
def ftorad(f):
    return f * 2.0 * math.pi

""" Returns ccw path of circle with right side touching origin. """
def circle( r = 1, pts = 24 ):
    res = []
    for i in range(1,pts+1):
        rad = ftorad(1.0*i/pts)
        px = math.cos(rad) - r
        py = math.sin(rad)
        res.append((px,py))
    return res

""" Rotates points in path 90d clockwise. """
def rotatecw( path ):
    res = []
    for (x,y) in path:
        res.append((y,-x))
    return res

""" Rotates points in path 90d counter-clockwise. """
def rotateccw( path ):
    res = []
    for (x,y) in path:
        res.append((-y,x))
    return res

""" Returns a list of difference between successive values. """
def diff( path ):
    res = []
    for i in range(1,len(path)):
        res.append(path[i]-path[i-1])
    return res

""" Print path in TyRANNY format. """
def printpath( path ):
    for (x,y) in path:
        sx = str(int(1000 * x) / 1000.0)
        sy = str(int(1000 * y) / 1000.0)
        sys.stdout.write("(" + sx + " " + sy + ") ")
    sys.stdout.write("\n")

""" Scale all points in a path """
def scalepath( path, factor ):
    return map( lambda (x,y):(x*factor,y*factor), path )

""" Svg move-to string to path in world coordinates. Assumes svg is
on a 90by70 canvas. """
def fromsvgstr( s ):
    strpairs = s[2:].split()
    svgcoords = []
    for p in strpairs:
        if p == "m": continue
        sx, sy = p.split(",")
        x, y = float(sx), float(sy)
        svgcoords.append((x, y))
    coords = []
    curx, cury = 0, 0
    for x, y in svgcoords[1:]:
        curx += x
        cury -= y
        coords.append((curx,cury))
    return scalepath(coords, 0.1)

""" b - a """
def subtractpath(b, a):
    assert(len(b) == len(a))
    res = []
    for i in range(len(b)):
        res.append((b[i][0]-a[i][0], b[i][1]-a[i][1]))
    return res

_ = 0
X = 1
bombs = [
    [_,_,_,_,_,X,_,_,_,_,_],
    [X,_,_,_,X,X,X,_,_,_,X],
    [X,_,_,_,X,X,X,_,_,_,X],
    [X,_,_,_,X,X,X,_,_,_,X],
    [X,_,_,_,X,X,X,_,_,_,X],
    [X,_,_,_,X,X,X,_,_,_,X],
    [X,_,X,_,_,_,_,_,X,_,X],
    [_,_,_,X,_,X,_,_,_,_,X],
    [_,X,_,_,_,X,_,X,_,_,_],
    [_,_,_,X,X,_,_,_,X,_,_],
    [_,X,_,_,_,_,X,_,_,_,X],
    [X,_,X,_,X,_,_,_,X,_,_],
    [_,_,_,_,_,X,X,_,_,X,_],
    [X,_,X,_,_,_,X,_,X,_,_],
    [_,_,_,X,_,X,_,_,_,_,X],
    [_,X,_,_,_,_,_,X,_,_,_],
    [_,_,_,X,X,_,_,_,X,_,_],
    [_,X,_,_,_,_,X,_,_,_,X],
    [X,_,X,_,X,_,_,_,X,_,_],
    [X,_,_,_,X,_,X,_,_,X,_],
    [X,_,X,_,_,_,X,_,X,_,_],
    [_,_,_,X,_,X,_,_,_,_,X],
    [_,X,_,_,_,_,_,X,_,_,_],
    [_,_,_,X,X,_,_,_,X,_,_],
    [_,X,_,_,_,_,X,_,_,_,X],
    [X,_,X,_,X,_,_,_,X,_,_],
    [X,X,X,X,X,_,X,X,X,X,X],
    [X,X,X,X,X,_,X,X,X,X,X],
    [_,_,_,_,_,_,_,_,_,_,_],
    [_,_,_,_,_,_,_,_,_,_,_],
    [_,_,X,X,X,X,X,X,X,_,_],
    [_,_,X,X,X,X,X,X,X,_,_],
    [_,_,_,_,_,_,_,_,_,_,_],
    [X,X,_,X,X,X,X,X,_,X,X],
    [X,X,_,X,X,X,X,X,_,X,X],
    [X,X,_,X,X,X,X,X,_,X,X],
]

offset = 0.50
space = 0.80
spacevert = 0.90
path = []
for i in range(len(bombs)):
  row = bombs[len(bombs)-i-1]
  for j in range(len(row)):
    if row[j]: 
      x = offset + space * j
      y = 8 + spacevert * i
      path.append((x,y))
s = ""
for (x,y) in path:
  s += "  (spawn UnstableProtector (" + str(x) + " " + str(y) + ")\n"
  s += "      (load-script move-down))\n"


#[0.3090169943749474, 0.27876825791752574, 0.22123174208247431, 0.14203952192020608, 0.04894348370484647, -0.04894348370484636, -0.1420395219202062, -0.2212317420824742, -0.27876825791752574, -0.3090169943749474, -0.30901699437494784, -0.2787682579175253, -0.22123174208247431, -0.1420395219202062, -0.04894348370484647, 0.04894348370484636, 0.14203952192020608, 0.2212317420824742, 0.27876825791752574, 0.3090169943749474]
#p = map(lambda (x,y):(x+1.5,y+4.0), [(0,0)] + paths.fromsvgstr(m))
#[int(1000*(random.random()*14-7 + 4.5))/1000.0 for i in range(273)]
#zip(start, map(lambda (x,y): (x+6,y), paths.subtractpath(p2,start[:80])))

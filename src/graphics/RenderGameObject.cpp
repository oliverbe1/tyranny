#include <SFML/Graphics.hpp>
#include <cmath>
#include "game/objects/GameObject.h"
#include "game/objects/PlayerShip.h"
#include "game/objects/Bullet.h"
#include "game/objects/Powerup.h"
#include "game/objects/Effect.h"
#include "game/objects/NovaDestroyer.h"
#include "game/objects/UnstableProtector.h"
#include "game/objects/SpiralSlinger.h"
#include "game/objects/Scout.h"
#include "game/objects/MadBomber.h"
#include "RenderGameObject.h"
#include "Assets.h"

namespace ty {
namespace sfml {

//////////////////////////////////////////////////////////////////////
RenderGameObject::RenderGameObject( sf::RenderWindow& window, int pixelsPerUnit )
: m_window(window), m_pixelsPerUnit(pixelsPerUnit) { }

//////////////////////////////////////////////////////////////////////
void RenderGameObject::applyTo( PlayerShip& ship )
{
    if( !ship.isDestroyed() ) {
        renderSpriteAtObjectLocation( assets().PLAYER, ship );
    }
}

//////////////////////////////////////////////////////////////////////
void RenderGameObject::applyTo( WeaponPowerup& powerup )
{
    if( powerup.getWeapon() == Weapon::SCATTERSHOT ) {
        renderSpriteAtObjectLocation( assets().POWERUP_SCATTERSHOT, powerup );
    } else {
        renderSpriteAtObjectLocation( assets().POWERUP_YCOMBINATOR, powerup );
    }
}

//////////////////////////////////////////////////////////////////////
void RenderGameObject::applyTo( SlowmoPowerup& powerup )
{
    renderSpriteAtObjectLocation( assets().POWERUP_SLOWMO, powerup );
}

//////////////////////////////////////////////////////////////////////
void RenderGameObject::applyTo( Bullet& bullet )
{
    sf::Sprite sprite;
    if     ( bullet.getGraphic() == Bullet::BALL   ) { sprite = assets().BULLET_BALL; }
    else if( bullet.getGraphic() == Bullet::BOMB   ) { sprite = assets().BULLET_BOMB; }
    else if( bullet.getGraphic() == Bullet::HEART  ) { sprite = assets().BULLET_HEART; }
    else if( bullet.getGraphic() == Bullet::PLAYER ) { sprite = assets().BULLET_PLAYER; }
    else if( bullet.getGraphic() == Bullet::TEAR   ) { sprite = assets().BULLET_TEAR; }
    else if( bullet.getGraphic() == Bullet::TOXIC  ) { sprite = assets().BULLET_TOXIC; }
    else                                             { sprite = assets().BULLET_BALL; }
    renderSpriteAtObjectLocation( sprite, bullet );
}

//////////////////////////////////////////////////////////////////////
void RenderGameObject::applyTo( NovaDestroyer& ship )
{
    renderSpriteAtObjectLocation( assets().ENEMY_NOVADESTROYER, ship );
}

//////////////////////////////////////////////////////////////////////
void RenderGameObject::applyTo( UnstableProtector& ship )
{
    renderSpriteAtObjectLocation( assets().ENEMY_UNSTABLEPROTECTOR, ship );
}

//////////////////////////////////////////////////////////////////////
void RenderGameObject::applyTo( SpiralSlinger& ship )
{
    renderSpriteAtObjectLocation( assets().ENEMY_SPIRALSLINGER, ship );
}

//////////////////////////////////////////////////////////////////////
void RenderGameObject::applyTo( Scout& ship )
{
    renderSpriteAtObjectLocation( assets().ENEMY_SCOUT, ship );
}

//////////////////////////////////////////////////////////////////////
void RenderGameObject::applyTo( MadBomber& ship )
{
    renderSpriteAtObjectLocation( assets().ENEMY_MADBOMBER, ship );
}

//////////////////////////////////////////////////////////////////////
void RenderGameObject::applyTo( Effect& effect )
{
    if( effect.getGraphic() == Effect::EXPLOSION ) {
        const int frameSize = 118;
        const int frameCnt  = 5;

        sf::Sprite sprite = assets().EFFECT_EXPLOSION;
        int x = frameSize * std::floor((frameCnt-0.01) * (1. - effect.getRemaining()));
        sprite.setTextureRect(sf::IntRect(x, 0, frameSize, frameSize));
        renderSpriteAtObjectLocation(sprite, effect );
    }
}

// Private
//////////////////////////////////////////////////////////////////////
void RenderGameObject::renderSpriteAtObjectLocation( sf::Sprite& sprite, GameObject& obj )
{
    int x = obj.getPosition().x * m_pixelsPerUnit;
    int y = obj.getPosition().y * m_pixelsPerUnit;
    int yi = m_window.getSize().y - y;
    sprite.setPosition(sf::Vector2f(x, yi));
    m_window.draw(sprite);
}

} // end ns sfml
} // end ns ty

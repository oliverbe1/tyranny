#include <algorithm>
#include "BulletFactory.h"
#include "Effect.h"
#include "GameObject.h"

namespace ty {

static const float kExplosionDuration = 0.3;

//////////////////////////////////////////////////////////////////////
Effect::Effect( Graphic g )
: GameObject(0)
, m_graphic(g)
, m_duration(kExplosionDuration)
, m_remaining(m_duration)
{ }

//////////////////////////////////////////////////////////////////////
Effect::Graphic Effect::getGraphic() const
{ return m_graphic; }

//////////////////////////////////////////////////////////////////////
float Effect::getRemaining() const
{ return m_remaining / m_duration; }

//////////////////////////////////////////////////////////////////////
void Effect::update( float timeDelta, BulletFactory& factory )
{ m_remaining = std::max(0.f, m_remaining - timeDelta); }

//////////////////////////////////////////////////////////////////////
void Effect::applyAction( GameObjectAction& a )
{ a.applyTo(*this); }

} // end ns

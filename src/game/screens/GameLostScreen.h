#ifndef INC_GAMELOSTSCREEN_H
#define INC_GAMELOSTSCREEN_H
#include <memory>
#include "game/Action.h"
#include "Screen.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
/// The screen when you've lost all your lives
//////////////////////////////////////////////////////////////////////
class GameLostScreen : public Screen
{
    float                   m_blockInputDuration;
    std::unique_ptr<Screen> m_nextScreen;

public:
    GameLostScreen( );

    // Screen implementation
    void processAction( const Action& action ) override;
    void update( float timeDelta ) override;
    std::unique_ptr<Screen> nextScreen() override;
    void applyAction( ScreenAction& a ) override;
};

} // end ns

#endif


#ifndef INC_GAMEFINISHEDSCREEN_H
#define INC_GAMEFINISHEDSCREEN_H
#include <memory>
#include "game/Action.h"
#include "Screen.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
/// The screen when you've finished the game.
//////////////////////////////////////////////////////////////////////
class GameFinishedScreen : public Screen
{
    float                   m_blockInputDuration;
    std::unique_ptr<Screen> m_nextScreen;

public:
    GameFinishedScreen( );

    // Screen implementation
    void processAction( const Action& action ) override;
    void update( float timeDelta ) override;
    std::unique_ptr<Screen> nextScreen() override;
    void applyAction( ScreenAction& a ) override;
};

} // end ns

#endif

#ifndef INC_ACTION_H
#define INC_ACTION_H

namespace ty {

//////////////////////////////////////////////////////////////////////
/// An action that must be processed by the game. Typically 
/// the result of user input.
//////////////////////////////////////////////////////////////////////
enum class Action  // A simple enumeration should do for now.
{
    LEFT,
    RIGHT,
    UP,
    DOWN,
    SHOOT_OR_SELECT,

    NEXT_LEVEL,
    RESTART_LEVEL,
    TOGGLE_GODMODE,
    SKIP_WAVE,
};

} // end ns

#endif

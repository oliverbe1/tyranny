Tyranny World Format (.tyw)
===========================
The Tyranny world format is an ascii-based file containing a definition of the
background tiles and one or more waves to be spawned.

For a detailed example with comments see "world1.tyw".

Top-level Components
====================
Background
----------
A world file must contain a background definition at the start of the file.
It consists of multiple rows of tiles:

    (background
      (T1 T2 .. T9)
      (U1 U2 .. U9)
      ...          )

A Tile may be one of:

    - S     ( a squared floor )
    - P     ( a blue plate )
    - p     ( a green plate )
    - B     ( a brown floor )
    - b     ( a brown floor with diagonal pattern )

Script
------
A script is a sequence of actions an enemy must perform. You can reuse a script 
multiple times by defining it in a script definition:

    (script script-name
        script-action)

where script-action is one of:
    
    - follow-path 
      (follow-path (0 1) (1 1) (2.5 2.5 2)) 

      Have unit follow a given path. Coordinates are relative to the unit's
      position when the script is started. A speed modifier may be added as 
      3rd element in the coordinate list.

    - idle
      (idle 2)

    - stop-shooting
      (stop-shooting)

    - start-shooting
      (start-shooting)

    - loop
      (loop 5 (follow-path (1 1) (0 0)))

    - do
      (do (stop-shooting)
          (follow-path (-1.5 0)))

    - load-script
      (load-script script-name)

Wave
----
Enemy units spawn in waves. A wave is defined by 

    (wave wave-action1 wave-action2 ..)

where a wave-action is one of:

    - wait
      (wait 3)

    - spawn
      (spawn unit-name (4 6) script)

      unit-name is one of: Scout, SpiralSlinger, NovaDestroyer,
      UnstableProtector, MadBomber.

    - powerup
      (powerup powerup-name (3 7))

      powerup-name is one of: ScatterShot, YCombinator, SlowMotion

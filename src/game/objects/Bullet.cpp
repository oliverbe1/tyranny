#include "Bullet.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
Bullet::Bullet( Graphic g, bool friendly, float damage, float radius )
: GameObject (radius)
, m_graphic  (g)
, m_friendly (friendly)
, m_damage   (damage)
{ }

//////////////////////////////////////////////////////////////////////
Bullet::Graphic Bullet::getGraphic() const
{ return m_graphic; }

bool Bullet::isFriendly() const
{ return m_friendly; }

float Bullet::getDamage() const
{ return m_damage; }

//////////////////////////////////////////////////////////////////////
void Bullet::applyAction( GameObjectAction& a )
{ a.applyTo(*this); }


} // end ns

#ifndef INC_RENDERGAMEOBJECT_H
#define INC_RENDERGAMEOBJECT_H
#include <SFML/Graphics.hpp>
#include "game/objects/GameObject.h"

namespace ty {
namespace sfml {

//////////////////////////////////////////////////////////////////////
/// Action for rendering game objects to a SFML window.
//////////////////////////////////////////////////////////////////////
class RenderGameObject : public GameObjectAction
{
    sf::RenderWindow& m_window;
    int m_pixelsPerUnit;

public:
    RenderGameObject( sf::RenderWindow& window, int pixelsPerUnit );

    virtual void applyTo( ty::PlayerShip& ship ) override;

    virtual void applyTo( ty::NovaDestroyer& ship ) override;
    virtual void applyTo( ty::UnstableProtector& ship ) override;
    virtual void applyTo( ty::SpiralSlinger& ship ) override;
    virtual void applyTo( ty::Scout& ship ) override;
    virtual void applyTo( ty::MadBomber& ship ) override;

    virtual void applyTo( ty::Bullet& bullet ) override;

    virtual void applyTo( ty::WeaponPowerup& powerup ) override;
    virtual void applyTo( ty::SlowmoPowerup& powerup ) override;

    virtual void applyTo( ty::Effect& effect ) override;

private:
    void renderSpriteAtObjectLocation( sf::Sprite& sprite, GameObject& obj );
};

} // end ns sfml
} // end ns ty

#endif

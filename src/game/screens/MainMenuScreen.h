#ifndef INC_MAINMENUSCREEN_H
#define INC_MAINMENUSCREEN_H
#include <memory>
#include "game/Action.h"
#include "Screen.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
/// The main menu screen.
//////////////////////////////////////////////////////////////////////
class MainMenuScreen : public Screen
{
public:
    enum Selection { HARD, HARDER };

    MainMenuScreen( );

    Selection getSelection() const;

    // Screen implementation
    void processAction( const Action& action ) override;
    void update( float timeDelta ) override;
    std::unique_ptr<Screen> nextScreen() override;
    void applyAction( ScreenAction& a ) override;

private:
    float                   m_blockInputDuration;
    Selection               m_selection;
    std::unique_ptr<Screen> m_nextScreen;

};

} // end ns

#endif

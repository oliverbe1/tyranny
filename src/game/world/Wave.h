#ifndef INC_WAVE_H
#define INC_WAVE_H
#include <list>
#include <memory>
#include <vector>
#include "game/objects/EnemyShip.h"
#include "game/objects/Powerup.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
/// Auxiliary data structure for the `World` class.
///
/// A Wave is a group of enemy units or powerups which will be 
/// spawned near in time. Levels are made up of several waves.
///
/// Players can take as much time to clear enemies in a wave as 
/// they want, not until all are gone will the next wave spawn.
//////////////////////////////////////////////////////////////////////
class Wave
{
public:
    //////////////////////////////////////////////////////////////////
    /// Constructs a new Wave. Use the `wait` and `addShip` methods 
    /// to populate the wave with enemy units.
    //////////////////////////////////////////////////////////////////
    Wave();

    //////////////////////////////////////////////////////////////////
    /// Wave should wait `time` seconds between spawning the previous 
    /// and the next ship or powerup.
    //////////////////////////////////////////////////////////////////
    void wait( float time );

    //////////////////////////////////////////////////////////////////
    /// Add a ship to be spawned to the wave.
    //////////////////////////////////////////////////////////////////
    void addShip( std::unique_ptr<EnemyShip> ship );

    //////////////////////////////////////////////////////////////////
    /// Add a powerup to be spawned to the wave.
    //////////////////////////////////////////////////////////////////
    void addPowerup( std::unique_ptr<Powerup> powerup );

    //////////////////////////////////////////////////////////////////
    /// Returns whether anything is left in the wave.
    //////////////////////////////////////////////////////////////////
    bool empty() const;

    //////////////////////////////////////////////////////////////////
    /// Reduces time until the next object may be spawned by `timeDelta`
    /// seconds.
    //////////////////////////////////////////////////////////////////
    void update( float timeDelta );

    //////////////////////////////////////////////////////////////////
    /// Retrieves and removes the first ship from the wave.
    /// If it is not yet time for a ship to spawn, a nullptr is returned.
    //////////////////////////////////////////////////////////////////
    std::unique_ptr<EnemyShip> removeReadyShip();

    //////////////////////////////////////////////////////////////////
    /// Retrieves and removes the first powerup from the wave.
    /// If it is not yet time for a powerup to spawn, a nullptr is returned.
    //////////////////////////////////////////////////////////////////
    std::unique_ptr<Powerup> removeReadyPowerup();

private:
    using DelayAndShip    = std::pair<float, std::unique_ptr<EnemyShip>>; 
    using DelayAndPowerup = std::pair<float, std::unique_ptr<Powerup>>; 

    enum Next { SHIP, POWERUP };
    std::list<DelayAndShip>    m_ships;
    std::list<DelayAndPowerup> m_powerups;
    std::list<Next>            m_order;
    float                      m_accumulatedTime;
};

} // end ns

#endif


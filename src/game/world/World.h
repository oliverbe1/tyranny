#ifndef INC_WORLD_H
#define INC_WORLD_H
#include <memory>
#include <vector>
#include "math/Vector.h"

#include "game/objects/GameObject.h"
#include "game/objects/EnemyShip.h"
#include "game/objects/Bullet.h"
#include "game/objects/Powerup.h"
#include "game/objects/PlayerShip.h"
#include "game/objects/Effect.h"

#include "Tile.h"
#include "Wave.h"

namespace ty {
class BulletFactory;

using Background = std::vector<std::vector<Tile>>;

//////////////////////////////////////////////////////////////////////
/// A world for the player to play through.
///
/// The world houses and keeps track of all active game objects, 
/// resolving collisions and spawning new ships when required.
/// It also takes care of the scrolling background.
//////////////////////////////////////////////////////////////////////
class World
{
    const static float     kBgScrollSpeed;
    const static Vector2f  kStartPos;
    const static Rectangle kBounds;
    const static float     kDefaultTimeMultiplier;
    const static float     kHarderTimeMultiplier;

    // Background
    Background        m_bg;
    float             m_bgPosition;
    // Waves
    std::vector<Wave> m_waves;
    size_t            m_currentWave;
    // Misc.
    float             m_slowmoFactor;
    float             m_slowmoDuration;

    // Objects
    std::vector<std::unique_ptr<EnemyShip>> m_enemies;
    std::vector<std::unique_ptr<Bullet>>    m_bullets;
    std::vector<std::unique_ptr<Powerup>>   m_powerups;
    std::vector<std::unique_ptr<Effect>>    m_effects;
    std::unique_ptr<PlayerShip>             m_player;

public:
    //////////////////////////////////////////////////////////////////
    /// Creates a new world that will spawn given waves when updated.
    //////////////////////////////////////////////////////////////////
    World( Background bg, std::vector<Wave> waves );

    //////////////////////////////////////////////////////////////////
    /// Returns a reference to the world's background.
    //////////////////////////////////////////////////////////////////
    const Background&        getBackground() const;

    //////////////////////////////////////////////////////////////////
    /// Returns the vertical scroll position of the background.
    /// One unit corresponds to the length of a tile.
    //////////////////////////////////////////////////////////////////
    float                    getBackgroundPosition() const;

    //////////////////////////////////////////////////////////////////
    /// Returns all game objects active in the world at this time.
    /// This includes the player's ship.
    //////////////////////////////////////////////////////////////////
    std::vector<GameObject*> getObjects() const;

    //////////////////////////////////////////////////////////////////
    /// Returns a pointer to the player's ship.
    //////////////////////////////////////////////////////////////////
    PlayerShip*              getPlayer() const;

    //////////////////////////////////////////////////////////////////
    /// Slow world-time by the given factor for given duration.
    //////////////////////////////////////////////////////////////////
    void slowTime( float factor, float duration );

    //////////////////////////////////////////////////////////////////
    /// Returns the currently active wave. Starts from 1.
    //////////////////////////////////////////////////////////////////
    int getCurrentWave() const;

    //////////////////////////////////////////////////////////////////
    /// Skip to the next wave, removing all current objects from the
    /// scene.
    //////////////////////////////////////////////////////////////////
    void skipWave();

    //////////////////////////////////////////////////////////////////
    /// Returns whether all waves of enemies have been cleared.
    //////////////////////////////////////////////////////////////////
    bool isCompleted() const;

    //////////////////////////////////////////////////////////////////
    /// Advances the state of the world by `timeDelta` real-time seconds.
    //////////////////////////////////////////////////////////////////
    void update( float timeDelta );

    friend class BulletFactory;

private:
    float convertToWorldTime( float time );
    void  checkCollisions();
    void  addEffect( Effect::Graphic g, Vector2f pos );
    void  pruneObjects();
};

} // end ns

#endif

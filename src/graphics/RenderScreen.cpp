#include <SFML/Graphics.hpp>
#include "game/screens/GameScreen.h"
#include "game/screens/MainMenuScreen.h"
#include "game/screens/GameLostScreen.h"
#include "game/screens/GameFinishedScreen.h"
#include "game/world/Tile.h"
#include "game/world/World.h"
#include "Assets.h"
#include "RenderGameObject.h"
#include "RenderScreen.h"

namespace ty {
namespace sfml {

//////////////////////////////////////////////////////////////////////
RenderScreen::RenderScreen( sf::RenderWindow& window, int pixelsPerUnit )
: m_window(window), m_pixelsPerUnit(pixelsPerUnit) { }

//////////////////////////////////////////////////////////////////////
void RenderScreen::applyTo( GameScreen& screen )
{
    const World& world = screen.getWorld();

    // 1. Render background
    unsigned numVertTiles = m_window.getSize().y / m_pixelsPerUnit;
    const Background& bg  = world.getBackground();
    float bgPos           = world.getBackgroundPosition();
    int   bgHeight        = (int)bg.size();
    int   bottomRow       = (int)floor(bgPos);
    int   topRow          = (int)floor(bgPos + numVertTiles);
    int   bottomOffset    = m_pixelsPerUnit * (bgPos - bottomRow);

    for( int i = bottomRow; i <= topRow; ++i ) {
        const auto& row = bg[i % bgHeight];
        for( int j = 0; j < (int)row.size(); ++j ) {
            Tile tile = row[j];
            sf::Sprite sprite;
            if     ( tile == Tile::SQUARES       ) { sprite = assets().TILE_SQUARES; }
            else if( tile == Tile::PLATE_BLUE    ) { sprite = assets().TILE_PLATE_BLUE; }
            else if( tile == Tile::PLATE_GREEN   ) { sprite = assets().TILE_PLATE_GREEN; }
            else if( tile == Tile::BROWN         ) { sprite = assets().TILE_BROWN; }
            else if( tile == Tile::BROWN_SLANTED ) { sprite = assets().TILE_BROWN_SLANTED; }
            // (x,y) in bottom-left coordinates
            int x = j * m_pixelsPerUnit;
            int y = (i - bottomRow) * m_pixelsPerUnit - bottomOffset;
            // convert to y in top-left window coordinates
            int yi = m_window.getSize().y - y - m_pixelsPerUnit;
            sprite.setPosition(x, yi);
            // Draw tile to screen
            m_window.draw(sprite);
        }
    }

    // 2. Render game objects
    RenderGameObject renderAction(m_window, m_pixelsPerUnit);
    for( auto pObj : world.getObjects() ) {
        pObj->applyAction(renderAction);
    }

    // 3. Render gui
    sf::Sprite lives;
    if( screen.getRemainingLives() == 0 ) { lives = assets().GUI_LIVES_0; }
    if( screen.getRemainingLives() == 1 ) { lives = assets().GUI_LIVES_1; }
    if( screen.getRemainingLives() == 2 ) { lives = assets().GUI_LIVES_2; }
    if( screen.getRemainingLives() == 3 ) { lives = assets().GUI_LIVES_3; }
    lives.setPosition(m_window.getSize().x, 0);
    m_window.draw(lives);

    m_window.display();
}

//////////////////////////////////////////////////////////////////////
void RenderScreen::applyTo( MainMenuScreen& screen )
{
    if( screen.getSelection() == MainMenuScreen::HARD ) {
        m_window.draw(assets().MENU_MAIN_HARD);
    } else {
        m_window.draw(assets().MENU_MAIN_HARDER);
    }
    m_window.display();
}

//////////////////////////////////////////////////////////////////////
void RenderScreen::applyTo( GameLostScreen& screen )
{
    m_window.draw(assets().MENU_LOST);
    m_window.display();
}

//////////////////////////////////////////////////////////////////////
void RenderScreen::applyTo( GameFinishedScreen& screen )
{
    m_window.draw(assets().MENU_WON);
    m_window.display();
}

} // end ns sfml
} // end ns ty


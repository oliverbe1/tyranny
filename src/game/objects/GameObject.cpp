#include "math/Vector.h"
#include "math/algorithm.h"
#include "GameObject.h"

namespace ty {
using namespace math;

//////////////////////////////////////////////////////////////////////
GameObject::GameObject( float radius ) 
: m_radius(radius) { }

GameObject::GameObject( Vector2f pos, Vector2f vel, float radius )
: m_pos(pos), m_vel(vel), m_radius(radius) { }

//////////////////////////////////////////////////////////////////////
Vector2f GameObject::getPosition() const
{ return m_pos; }

void GameObject::setPosition( Vector2f pos )
{ m_pos = pos; }

//////////////////////////////////////////////////////////////////////
Vector2f GameObject::getVelocity() const
{ return m_vel; }

void GameObject::setVelocity( Vector2f vel )
{ m_vel = vel; }

//////////////////////////////////////////////////////////////////////
float GameObject::getRadius() const
{ return m_radius; }

//////////////////////////////////////////////////////////////////////
bool GameObject::intersects( const GameObject& other ) const
{ return distance(m_pos, other.m_pos) < (m_radius + other.m_radius); }

//////////////////////////////////////////////////////////////////////
void GameObject::update( float timeDelta, BulletFactory& factory )
{ m_pos += timeDelta * m_vel; }


} // end ns

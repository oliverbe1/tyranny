#ifndef INC_NOVADESTROYER_H
#define INC_NOVADESTROYER_H
#include <memory>
#include "BulletFactory.h"
#include "EnemyShip.h"

namespace ty {
class GameObjectAction;

//////////////////////////////////////////////////////////////////////
/// An enemy ship shooting out large novas of bullets periodically.
//////////////////////////////////////////////////////////////////////
class NovaDestroyer : public EnemyShip
{
public:
    NovaDestroyer( std::unique_ptr<Script> script );
    virtual void applyAction( GameObjectAction& a ) override;
protected:
    virtual void shoot( BulletFactory& factory ) override;
};

} // end ns

#endif

#include <algorithm>
#include <memory>
#include <vector>
#include "math/algorithm.h"
#include "math/Vector.h"
#include "Script.h"
#include "EnemyShip.h"

namespace ty {
using namespace math;

//////////////////////////////////////////////////////////////////////
//                           Sequence
//////////////////////////////////////////////////////////////////////
Sequence::Sequence() : m_currentScript(0) { }

//////////////////////////////////////////////////////////////////////
void Sequence::addScript( unique_ptr<Script> script )
{
    m_scripts.push_back(std::move(script));
}

//////////////////////////////////////////////////////////////////////
void Sequence::execute( EnemyShip& ship, float timeDelta )
{
    if( isFinished() ) { 
        return; 
    } else {
        auto& curScript = m_scripts[m_currentScript];
        curScript->execute(ship, timeDelta);
        if( curScript->isFinished() ) {
            m_currentScript++;
            execute(ship, timeDelta);
        }
    }
}

//////////////////////////////////////////////////////////////////////
void Sequence::reset()
{
    for( auto& ps : m_scripts ) {
        ps->reset();
    }
    m_currentScript = 0;
}

//////////////////////////////////////////////////////////////////////
bool Sequence::isFinished() const
{ return m_currentScript >= m_scripts.size(); }

//////////////////////////////////////////////////////////////////////
unique_ptr<Script> Sequence::clone() const
{
    Sequence* s = new Sequence();
    for( auto& ps : m_scripts ) {
        s->addScript( ps->clone() );
    }
    return unique_ptr<Script>(s);
}

//////////////////////////////////////////////////////////////////////
//                           Loop
//////////////////////////////////////////////////////////////////////
Loop::Loop( unique_ptr<Script> script, int numIterations ) 
: m_script(std::move(script))
, m_numIterations(numIterations)
, m_remainingIterations(numIterations) 
{ }

//////////////////////////////////////////////////////////////////////
void Loop::execute( EnemyShip& ship, float timeDelta )
{
    if( isFinished() ) { 
        return; 
    } else {
        m_script->execute(ship, timeDelta);
        if( m_script->isFinished() ) {
            m_script->reset();
            m_remainingIterations--;
            execute(ship, timeDelta);
        }
    }
}

//////////////////////////////////////////////////////////////////////
void Loop::reset()
{
    m_script->reset();
    m_remainingIterations = m_numIterations;
}

//////////////////////////////////////////////////////////////////////
bool Loop::isFinished() const
{ return m_remainingIterations <= 0; }

//////////////////////////////////////////////////////////////////////
unique_ptr<Script> Loop::clone() const
{
    Loop* s = new Loop(m_script->clone(), m_numIterations);
    s->m_remainingIterations = m_remainingIterations;
    return unique_ptr<Script>(s);
}


//////////////////////////////////////////////////////////////////////
//                           FollowPath
//////////////////////////////////////////////////////////////////////
// Note: the ship's original position will always be made part of the absolute path.
// Thus the path length (and multipliers length) will be one more than the relative
// path provided.
// This way we always know the direction the ship should be travelling in when moving
// to its current goal.

//////////////////////////////////////////////////////////////////////
FollowPath::FollowPath( vector<Vector2f> relativePath )
: m_relativePath      (std::move(relativePath))
, m_speedMultipliers  (m_relativePath.size()+1, 1)
, m_absPathCalculated (false)
, m_curGoal           (1) 
{ }

//////////////////////////////////////////////////////////////////////
FollowPath::FollowPath( vector<Vector2f> relativePath,
                        vector<float> speedMultipliers )
: FollowPath (relativePath)
{ 
    for( size_t i = 0; i+1 < m_speedMultipliers.size() && i < speedMultipliers.size(); ++i ) {
        m_speedMultipliers[i+1] = speedMultipliers[i];
    }
}

//////////////////////////////////////////////////////////////////////
void FollowPath::execute( EnemyShip& ship, float timeDelta )
{
    if( !m_absPathCalculated ) { calculateAbsolutePath(ship); }
    if( isFinished() ) { return; }

    Vector2f curPos   = ship.getPosition();
    Vector2f goalPos  = m_absolutePath[m_curGoal];
    Vector2f prevGoal = m_absolutePath[m_curGoal - 1];
    Vector2f origDiff = goalPos - prevGoal;
    Vector2f curDiff  = goalPos - curPos;

    // If ship has passed its goal, or is near enough as makes no difference,
    //   move on to the next goal.
    if( !same_direction(curDiff, origDiff) || length(curDiff) < 0.02 ) {
        ship.setPosition(goalPos);
        m_curGoal++;
        execute(ship,timeDelta);
    // Otherwise, direct ship to current goal.
    } else {
        float speed = m_speedMultipliers[m_curGoal] * ship.getBaseSpeed();
        Vector2f newVel = normalize(origDiff) * speed;
        ship.setVelocity(newVel);
    }
}

//////////////////////////////////////////////////////////////////////
void FollowPath::reset() 
{ m_absPathCalculated = false; }

//////////////////////////////////////////////////////////////////////
bool FollowPath::isFinished() const
{ return m_absPathCalculated && m_curGoal >= m_absolutePath.size(); }

//////////////////////////////////////////////////////////////////////
unique_ptr<Script> FollowPath::clone() const
{
    FollowPath* s = new FollowPath( m_relativePath );
    s->m_speedMultipliers = m_speedMultipliers;
    s->m_absPathCalculated = m_absPathCalculated;
    s->m_curGoal = m_curGoal;
    return unique_ptr<Script>(s);
}

// Private Members
//////////////////////////////////////////////////////////////////////
void FollowPath::calculateAbsolutePath( EnemyShip& ship )
{
    m_absolutePath.clear();
    m_absolutePath.push_back( ship.getPosition() );
    for( const auto& dv : m_relativePath ) {
        m_absolutePath.push_back( ship.getPosition() + dv );
    }
    m_curGoal = 1;
    m_absPathCalculated = true;
}


//////////////////////////////////////////////////////////////////////
//                           Idle
//////////////////////////////////////////////////////////////////////
Idle::Idle( float duration ) 
: m_duration(duration)
, m_remaining(duration)
{ }

//////////////////////////////////////////////////////////////////////
void Idle::execute( EnemyShip& ship, float timeDelta )
{
    if( isFinished() ) { 
        return; 
    } else {
        ship.setVelocity(Vector2f(0,0));
        m_remaining = std::max(0.f, m_remaining - timeDelta);
    }
}

//////////////////////////////////////////////////////////////////////
void Idle::reset()
{
    m_remaining = m_duration;
}

//////////////////////////////////////////////////////////////////////
bool Idle::isFinished() const
{ return m_remaining <= 0; }

//////////////////////////////////////////////////////////////////////
unique_ptr<Script> Idle::clone() const
{
    Idle* s = new Idle( m_duration );
    s->m_remaining = m_remaining;
    return unique_ptr<Script>(s);
}


//////////////////////////////////////////////////////////////////////
//                           StopShooting
//////////////////////////////////////////////////////////////////////
StopShooting::StopShooting() 
: m_done(false) { }

//////////////////////////////////////////////////////////////////////
void StopShooting::execute( EnemyShip& ship, float timeDelta )
{
    if( isFinished() ) { 
        return; 
    } else {
        ship.stopShooting();
        m_done = true;
    }
}

//////////////////////////////////////////////////////////////////////
void StopShooting::reset()
{ m_done = false; }

//////////////////////////////////////////////////////////////////////
bool StopShooting::isFinished() const
{ return m_done; }

//////////////////////////////////////////////////////////////////////
unique_ptr<Script> StopShooting::clone() const
{
    StopShooting* s = new StopShooting();
    s->m_done = m_done;
    return unique_ptr<Script>(s);
}

//////////////////////////////////////////////////////////////////////
//                           StartShooting
//////////////////////////////////////////////////////////////////////
StartShooting::StartShooting() 
: m_done(false) { }

//////////////////////////////////////////////////////////////////////
void StartShooting::execute( EnemyShip& ship, float timeDelta )
{
    if( isFinished() ) { 
        return; 
    } else {
        ship.startShooting();
        m_done = true;
    }
}

//////////////////////////////////////////////////////////////////////
void StartShooting::reset()
{ m_done = false; }

//////////////////////////////////////////////////////////////////////
bool StartShooting::isFinished() const
{ return m_done; }

//////////////////////////////////////////////////////////////////////
unique_ptr<Script> StartShooting::clone() const
{
    StartShooting* s = new StartShooting();
    s->m_done = m_done;
    return unique_ptr<Script>(s);
}

} // end ns

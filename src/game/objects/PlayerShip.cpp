#include <algorithm>
#include "math/algorithm.h"
#include "math/Rectangle.h"
#include "math/Vector.h"
#include "Bullet.h"
#include "BulletFactory.h"
#include "GameObject.h"
#include "PlayerShip.h"

namespace ty {
using namespace ty::math;

const float PlayerShip::kRadius           = 0.17;
const float PlayerShip::kHorizontalAcc    = 32;
const float PlayerShip::kVerticalAcc      = 25;
const float PlayerShip::kHorizontalDeAcc  = 30;
const float PlayerShip::kVerticalDeAcc    = 23;
const float PlayerShip::kMaxHorizontalVel = 2.65;
const float PlayerShip::kMaxVerticalVel   = 2.0;
const float PlayerShip::kWeaponStandardCD = 0.15;  // cooldowns
const float PlayerShip::kWeaponScatterCD  = 0.035;
const float PlayerShip::kWeaponYCombCD    = 0.25;
const Rectangle PlayerShip::kMovementBounds = {0.3,5,8.4,4.6};

//////////////////////////////////////////////////////////////////////
//                          Public Members
//////////////////////////////////////////////////////////////////////
PlayerShip::PlayerShip()
: PlayerShip({0,0}, {0,0}) { }

PlayerShip::PlayerShip( Vector2f pos, Vector2f vel )
: GameObject (pos, vel, kRadius)
, m_acc      (0,0)
, m_weapon   (Weapon::STANDARD)
, m_cooldown (kWeaponStandardCD)
, m_fire     (false)
, m_destroyed(false)
{ }

//////////////////////////////////////////////////////////////////////
void PlayerShip::moveLeft()
{ m_acc.x = -kHorizontalAcc; }

void PlayerShip::moveRight()
{ m_acc.x = kHorizontalAcc; }

void PlayerShip::moveUp()
{ m_acc.y = kVerticalAcc; }

void PlayerShip::moveDown()
{ m_acc.y = -kVerticalAcc; }

//////////////////////////////////////////////////////////////////////
void PlayerShip::shoot()
{ m_fire = true; }

void PlayerShip::changeWeapon( Weapon weapon )
{ m_weapon = weapon; }

//////////////////////////////////////////////////////////////////////
void PlayerShip::destroy()
{ m_destroyed = true; }

bool PlayerShip::isDestroyed() const
{ return m_destroyed; }

//////////////////////////////////////////////////////////////////////
void PlayerShip::update( float timeDelta, BulletFactory& factory )
{
    updateMovement(timeDelta, factory);
    fireWeapons(timeDelta, factory);
}

//////////////////////////////////////////////////////////////////////
void PlayerShip::applyAction( GameObjectAction& a )
{ a.applyTo(*this); }


//////////////////////////////////////////////////////////////////////
//                         Private Members
//////////////////////////////////////////////////////////////////////
void PlayerShip::updateMovement( float timeDelta, BulletFactory& factory )
{
    Vector2f oldVel = getVelocity();

    // Apply deacceleration when ship stops moving
    if( m_acc.x == 0 ) {
        if( abs(oldVel.x) < 0.05 ) { oldVel.x = 0; }
        m_acc.x = -signum(oldVel.x) * kHorizontalDeAcc;
    }
    if( m_acc.y == 0 ) {
        if( abs(oldVel.y) < 0.05 ) { oldVel.y = 0; }
        m_acc.y = -signum(oldVel.y) * kVerticalDeAcc;
    }

    // Calculate new ship velocity
    Vector2f newVel = timeDelta * m_acc + oldVel;
    if     ( newVel.x < -kMaxHorizontalVel ) { newVel.x = -kMaxHorizontalVel; }
    else if( newVel.x >  kMaxHorizontalVel ) { newVel.x =  kMaxHorizontalVel; }
    if     ( newVel.y < -kMaxVerticalVel   ) { newVel.y = -kMaxVerticalVel;   }
    else if( newVel.y >  kMaxVerticalVel   ) { newVel.y =  kMaxVerticalVel;   }

    // Apply new velocity to ship
    setVelocity(newVel);
    GameObject::update(timeDelta, factory);
    checkBounds();

    // Reset acceleration
    m_acc = {0,0};
}

//////////////////////////////////////////////////////////////////////
void PlayerShip::fireWeapons( float timeDelta, BulletFactory& factory )
{
    m_cooldown = std::max(0.f, m_cooldown - timeDelta);
    if( m_fire && m_cooldown == 0 ) { 
        Vector2f pos = getPosition() + Vector2f(0, getRadius()*1.4);
        Vector2f vel = Vector2f(0, 5.5);
        switch( m_weapon ) {
        case Weapon::STANDARD:
            factory.spawnFriendlyBullet(pos, vel, Bullet::PLAYER, 5, 0.1);
            m_cooldown = kWeaponStandardCD;
            break;
        case Weapon::SCATTERSHOT:
            vel = Vector2f(randfloat()*2.2-1.1, 4.25);
            factory.spawnFriendlyBullet(pos, vel, Bullet::PLAYER, 1.9, 0.1);
            m_cooldown = kWeaponScatterCD;
            break;
        case Weapon::YCOMBINATOR:
            vel = Vector2f(0, 6.0);
            factory.spawnFriendlyBullet(pos, vel + Vector2f(-2,0), Bullet::PLAYER, 3.5, 0.1);
            factory.spawnFriendlyBullet(pos, vel, Bullet::PLAYER, 8, 0.1);
            factory.spawnFriendlyBullet(pos, vel + Vector2f(2,0), Bullet::PLAYER, 3.5, 0.1);
            m_cooldown = kWeaponYCombCD;
            break;
        }
    }
    m_fire = false;
}

//////////////////////////////////////////////////////////////////////
void PlayerShip::checkBounds()
{
    Vector2f pos = getPosition();
    float right  = kMovementBounds.left + kMovementBounds.width;
    float bottom = kMovementBounds.top - kMovementBounds.height;
    if( pos.x < kMovementBounds.left ) { pos.x = kMovementBounds.left; }
    if( pos.x > right                ) { pos.x = right;                }
    if( pos.y < bottom               ) { pos.y = bottom;               }
    if( pos.y > kMovementBounds.top  ) { pos.y = kMovementBounds.top;  }
    setPosition(pos);
}

} // end ns




#include <memory>
#include "game/Action.h"
#include "game/GlobalSettings.h"
#include "GameScreen.h"
#include "MainMenuScreen.h"
#include "Screen.h"

namespace ty {

//////////////////////////////////////////////////////////////////////
MainMenuScreen::MainMenuScreen() 
: m_blockInputDuration(1.0)
, m_selection (HARD)
, m_nextScreen(nullptr)
{ }

//////////////////////////////////////////////////////////////////////
MainMenuScreen::Selection MainMenuScreen::getSelection() const
{ return m_selection; }

//////////////////////////////////////////////////////////////////////
void MainMenuScreen::processAction( const Action& action )
{
    if( action == Action::UP ) {
        m_selection = HARD;
    } else 
    if( action == Action::DOWN ) {
        m_selection = HARDER;
    } else 
    if( action == Action::SHOOT_OR_SELECT && m_blockInputDuration <= 0 ) {
        if( m_selection == HARD   ) { globalSettings().setDifficulty(GlobalSettings::Difficulty::HARD); }
        if( m_selection == HARDER ) { globalSettings().setDifficulty(GlobalSettings::Difficulty::HARDER); }
        m_nextScreen = std::unique_ptr<Screen>(new GameScreen(1));
    }
}

//////////////////////////////////////////////////////////////////////
void MainMenuScreen::update( float timeDelta )
{
    m_blockInputDuration -= timeDelta;
}

//////////////////////////////////////////////////////////////////////
std::unique_ptr<Screen> MainMenuScreen::nextScreen()
{
    return std::move(m_nextScreen);
}

//////////////////////////////////////////////////////////////////////
void MainMenuScreen::applyAction( ScreenAction& a )
{
    a.applyTo(*this);
}

} // end ns

